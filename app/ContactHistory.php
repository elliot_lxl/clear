<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactHistory extends Model
{
    //
    protected $table = 'contact_history';

    public $timestamps = false;
}
