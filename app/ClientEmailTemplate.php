<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientEmailTemplate extends Model
{
    //
    protected $table = 'email_templates';

    public $timestamps = false;

    public function templateContent(){
        return EmailTemplate::where('template_id',$this->id)->first();
    }
}
