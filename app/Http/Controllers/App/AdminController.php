<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\BlankonController;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use View;

class AdminController extends BlankonController
{
    //
    //
    public function __construct() {

        parent::__construct();

        $this->setApp();

        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
        ];


        // page level plugins
        $this->js['plugins'] = [];
    }

    /**
     * @return mixed
     */
    public function index()
    {
        // css class for body
        $this->bodyClass = 'page-footer-fixed';

        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'admin/css/custom.css',
        ];

        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'Application Admin');
        View::share('bodyClass', $this->bodyClass);


        //Application Admin page
        $group = Auth::user()->group_id;

        return view('app.dashboard',compact('group'));
    }

}
