<?php

namespace App\Http\Controllers\App;

use App\Client;
use App\Country;
use App\Currency;
use App\Http\Controllers\BlankonController;
use App\PaymentMethod;
use App\PaymentProvider;
use Illuminate\Http\Request;

use App\Http\Requests;
use View;

class ClientController extends BlankonController
{
    //
    public function __construct() {

        parent::__construct();

        $this->setApp();

        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
        ];

        // page level plugins
        $this->js['plugins'] = [];
    }

    public function showAddPage(){
        $this->bodyClass = 'page-footer-fixed';

        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
            'global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
            'global/plugins/bower_components/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
            'global/plugins/bower_components/chosen_v1.2.0/chosen.min.css'
        ];
        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'admin/css/custom.css',
        ];

        // page level plugins
        $this->js['plugins'] = [
            'global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js',
            'global/plugins/bower_components/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js',
            'global/plugins/bower_components/holderjs/holder.js',
            'global/plugins/bower_components/bootstrap-maxlength/bootstrap-maxlength.min.js',
            'global/plugins/bower_components/jquery-autosize/jquery.autosize.min.js',
            'global/plugins/bower_components/chosen_v1.2.0/chosen.jquery.min.js'
        ];

        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/pages/blankon.form.element.js',
            'admin/js/demo.js'
        ];

        $countries  = Country::all();
        $currencies = Currency::all();
        $payments   = PaymentProvider::all();
        $methods    = PaymentMethod::all();

        // pass variable to view
        View::share('css',        $this->css);
        View::share('js',         $this->js);
        View::share('title',      'Add a new client');
        View::share('bodyClass',  $this->bodyClass);
        View::share('countries',  $countries);
        View::share('currencies', $currencies);
        View::share('providers', $payments);
        View::share('methods', $methods);


        return view('app/addclient');
    }

    public function postAdd(){
        var_dump($_POST);



        $client = new Client();
        $client->display_name          = $_POST['display_name'];
        $client->client_name           = $_POST['client_name'];
        $client->contact_email_address = $_POST['contact_email_address'];
        $client->client_status         = $_POST['client_status'];
        $client->country_id            = $_POST['country_id'];
        $client->currency_id           = $_POST['currency_id'];

        $client->save();

        $client->addPaymentProvider($_POST['payment_provider_id']);
        foreach ($_POST['payment_method_id'] as $id)
            $client->addPaymentMethod($id);

        

    }

    public function showList(){
        $this->bodyClass = 'page-footer-fixed';
        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
            'global/plugins/bower_components/datatables/css/dataTables.bootstrap.css',
            'global/plugins/bower_components/datatables/css/datatables.responsive.css',
            'global/plugins/bower_components/fuelux/dist/css/fuelux.min.css'
        ];

        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'admin/css/custom.css',
        ];

        // page level plugins
        $this->js['plugins'] = [
            'global/plugins/bower_components/datatables/js/jquery.dataTables.min.js',
            'global/plugins/bower_components/datatables/js/dataTables.bootstrap.js',
            'global/plugins/bower_components/datatables/js/datatables.responsive.js',
            'global/plugins/bower_components/fuelux/dist/js/fuelux.min.js'
        ];
        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/app/clients.table.js',
            'admin/js/demo.js'
        ];



        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'Clients');
       // View::share('clients',$clients);

        View::share('bodyClass',  $this->bodyClass);



        return view('app.clientslist');
    }

    public function clientsJson(){
        return $clients = Client::all()->toJson();
    }

    public function clientView($id){

        $client = Client::all()->find($id);

        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
            'global/plugins/bower_components/datatables/css/dataTables.bootstrap.css',
            'global/plugins/bower_components/datatables/css/datatables.responsive.css',
            'global/plugins/bower_components/fuelux/dist/css/fuelux.min.css'
        ];


        // page level plugins
        $this->js['plugins'] = [
            'global/plugins/bower_components/datatables/js/jquery.dataTables.min.js',
            'global/plugins/bower_components/datatables/js/dataTables.bootstrap.js',
            'global/plugins/bower_components/datatables/js/datatables.responsive.js',
            'global/plugins/bower_components/fuelux/dist/js/fuelux.min.js'
        ];

        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'admin/css/custom.css',
        ];

        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/pages/blankon.table.js',
            'admin/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'PROFILE | '.$client->display_name);
        View::share('client', $client);
        View::share('client_id', $id);
        return view('app/client-profile');
    }

    
}
