<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\BlankonController;
use App\User;
use App\UserGroup;
use Illuminate\Http\Request;
use View;
use App\Http\Requests;

class UserController extends BlankonController
{
    //
    public function __construct() {

        parent::__construct();

        $this->setApp();

        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
        ];

        // page level plugins
        $this->js['plugins'] = [];
    }

    public function showList(){
        $this->bodyClass = 'page-footer-fixed';

        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
            'global/plugins/bower_components/datatables/css/dataTables.bootstrap.css',
            'global/plugins/bower_components/datatables/css/datatables.responsive.css',
            'global/plugins/bower_components/fuelux/dist/css/fuelux.min.css'
        ];

        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'admin/css/custom.css',
        ];

        // page level plugins
        $this->js['plugins'] = [
            'global/plugins/bower_components/datatables/js/jquery.dataTables.min.js',
            'global/plugins/bower_components/datatables/js/dataTables.bootstrap.js',
            'global/plugins/bower_components/datatables/js/datatables.responsive.js',
            'global/plugins/bower_components/fuelux/dist/js/fuelux.min.js'
        ];
        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/app/users.table.js',
            'admin/js/demo.js'
        ];



        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'Users');

        // View::share('clients',$clients);

        View::share('bodyClass',  $this->bodyClass);

        return view('app.userslist');
    }

    function usersJson(){
        $users = [];
        foreach (User::all() as $tmp){
            $user = $tmp->toArray();
            $user['group'] = $tmp->groupName();
            $users[] =$user;
        }
        return json_encode($users);
    }

    function view($user_id){
        $user  = User::all()->find($user_id);

        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
            'global/plugins/bower_components/select2-ng/select2.css',
            'global/plugins/bower_components/select2-ng/select2-bootstrap.css',
            'global/plugins/bower_components/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
            'global/plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css',
            'global/plugins/bower_components/x-editable/dist/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css',
            'global/plugins/bower_components/x-editable/dist/inputs-ext/address/address.css',
        ];

        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'admin/css/custom.css',
        ];

        // page level plugins
        $this->js['plugins'] = [
            'global/plugins/bower_components/jquery-mockjax/jquery.mockjax.js',
            'global/plugins/bower_components/moment/min/moment.min.js',
            'global/plugins/bower_components/select2-ng/select2.min.js',
            'global/plugins/bower_components/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
            'global/plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js',
            'global/plugins/bower_components/x-editable/dist/inputs-ext/typeaheadjs/lib/typeahead.js',
            'global/plugins/bower_components/x-editable/dist/inputs-ext/typeaheadjs/typeaheadjs.js',
            'global/plugins/bower_components/x-editable/dist/inputs-ext/address/address.js',
        ];

        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/app/user.form.xeditable.js',
            'admin/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'User: '.$user->name);
        View::share('user', $user);

        return view('app.user-view');
    }

    public function postUser($id){
        return response($_POST['name']);
    }
}
