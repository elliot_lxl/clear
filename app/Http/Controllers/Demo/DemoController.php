<?php

namespace App\Http\Controllers\Demo;

use App\Http\Controllers\BlankonController;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use PhpSpec\Exception\Exception;
use View;

class DemoController extends BlankonController
{
    //
    /*
      |--------------------------------------------------------------------------
      | DashboardController
      |--------------------------------------------------------------------------
     */

    public function __construct() {

        parent::__construct();

        $this->setApp();
        $this->bodyClass = 'page-footer-fixed';
        View::share('bodyClass', $this->bodyClass);


        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
            'global/plugins/bower_components/dropzone/downloads/css/dropzone.css',
            'global/plugins/bower_components/jquery.gritter/css/jquery.gritter.css'
        ];

        // page level plugins
        $this->js['plugins'] = [];
    }

    /**
     * Show the application dashboard screen to the user.
     *
     * @return Response
     */
    public function index() {

        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => ''],
            'admin/css/custom.css',
        ];

        // page level plugins
        $this->js['plugins'] = [
            'global/plugins/bower_components/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js',
            'global/plugins/bower_components/flot/jquery.flot.js',
            'global/plugins/bower_components/flot/jquery.flot.spline.min.js',
            'global/plugins/bower_components/flot/jquery.flot.categories.js',
            'global/plugins/bower_components/flot/jquery.flot.tooltip.min.js',
            'global/plugins/bower_components/flot/jquery.flot.resize.js',
            'global/plugins/bower_components/flot/jquery.flot.pie.js',
            'global/plugins/bower_components/dropzone/downloads/dropzone.min.js',
            'global/plugins/bower_components/jquery.gritter/js/jquery.gritter.min.js',
            'global/plugins/bower_components/skycons-html5/skycons.js'
        ];

        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/demo/demo.dashboard.js',
            'admin/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'DASHBOARD | BLANKON - Fullpack Admin Theme');

        return view('demo/index');
    }

    public function showDebts(){
        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'admin/css/pages/frontend-themes.css',
            'admin/css/custom.css',
        ];

        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'BLOG LIST | BLANKON - Fullpack Admin Theme');

        return view('demo/debts-list');
    }

    public function sms(){
        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
            'global/plugins/bower_components/bootstrap-wysihtml5/src/bootstrap-wysihtml5.css',
            'global/plugins/bower_components/bootstrap-fileupload/css/bootstrap-fileupload.min.css'
        ];

        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'admin/css/pages/mail.css',
            'admin/css/custom.css',
        ];

        // page level plugins
        $this->js['plugins'] = [
            'global/plugins/bower_components/bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.min.js',
            'global/plugins/bower_components/bootstrap-wysihtml5/src/bootstrap-wysihtml5.js',
            'global/plugins/bower_components/bootstrap-fileupload/js/bootstrap-fileupload.min.js',
        ];

        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/pages/blankon.mail.js',
            'admin/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'Send SMS');

        return view('demo/sms');
    }

    public function sendSms(){
        $to = $_POST['to'];
        $content = $_POST['content'];
        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'admin/css/pages/mail.css',
            'admin/css/custom.css',
        ];

        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/pages/blankon.mail.js',
            'admin/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('to', $to);
        View::share('content', $content);
        View::share('title', 'SMS VIEW');

        $AccountSid = "AC3d890620cc29b8ac8f2347161e0da970";
        $AuthToken = "4e07a0e8bdd103d585d4d482b6acba7c";

        $client = new \Services_Twilio($AccountSid, $AuthToken);
        try {
            $message = $client->account->messages->create(array(
                'From' => "+61428791088",
                'To'   => $to,
                "Body" => $content,
            ));

            View::share('success_msg','Message send');
        }catch (Exception $e){
            //var_dump($e);
            View::share('error_msg','Failed to send message');
        }

        return view('demo/sms-view');
    }

    public function payment($token){
        if($token!='hi3785hno'){
            $error_msg = 'Invalid link token.';
            return view('demo/payment',compact('error_msg'));
        }

        if(isset($_POST['amount'])) {
            $amount = $_POST['amount'];

        }else {
            $amount = '299.10';
        }

        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => ''],
            'admin/css/custom.css',
        ];

        // page level plugins
        $this->js['plugins'] = [
            'global/plugins/bower_components/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js',
            'global/plugins/bower_components/flot/jquery.flot.js',
            'global/plugins/bower_components/flot/jquery.flot.spline.min.js',
            'global/plugins/bower_components/flot/jquery.flot.categories.js',
            'global/plugins/bower_components/flot/jquery.flot.tooltip.min.js',
            'global/plugins/bower_components/flot/jquery.flot.resize.js',
            'global/plugins/bower_components/flot/jquery.flot.pie.js',
            'global/plugins/bower_components/dropzone/downloads/dropzone.min.js',
            'global/plugins/bower_components/jquery.gritter/js/jquery.gritter.min.js',
            'global/plugins/bower_components/skycons-html5/skycons.js'
        ];

        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/demo/demo.dashboard.js',
            'admin/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'Payment');
        return view('demo/payment',compact('amount'));
    }

}
