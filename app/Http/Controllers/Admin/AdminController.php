<?php

namespace App\Http\Controllers\Admin;

use App\Client;
use App\Http\Controllers\BlankonController;
use App\Rule;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use View;
use Hash;

class AdminController extends BlankonController
{
    //
    private $clients;
    public function __construct() {

        parent::__construct();

        $this->setApp();

        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
        ];

        $user = Auth::user();
        // page level plugins
        $this->js['plugins'] = [];
        $group = Auth::user()->group_id;
        View::share('group',$group);
        $this->clients = $user->clients();
        // css class for body
        $this->bodyClass = 'page-footer-fixed';
        View::share('bodyClass', $this->bodyClass);
    }

    /**
     * @return mixed
     */
    public function index()
    {


        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'admin/css/custom.css',
        ];

        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'Dashboard');



        //Application Admin page


        return view('admin.dashboard');
    }
    //Start of user relating functions
    public function addUser(){
        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'admin/css/custom.css',
        ];

        // page level plugins
        $this->js['plugins'] = [
            'global/plugins/bower_components/chosen_v1.2.0/chosen.jquery.min.js',
            'global/plugins/bower_components/jquery-mockjax/jquery.mockjax.js',
            'global/plugins/bower_components/jquery-validation/dist/jquery.validate.min.js'
        ];

        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/pages/blankon.form.validation.js',
            'admin/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('clients', $this->clients);
        View::share('title', 'Add User | Clearpay');

        return view('admin.add-user');
    }

    public function postAddUser(){
        $user = User::create($_POST);
        $user->password = Hash::make($_POST['password']);
        $user->group_id = 2;

        if($user->save()){
            $user->linkToClient($_POST['client_id'],$_POST['group_id']);
            var_dump($user);
        }


    }

    public function usersList() {



        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'admin/css/custom.css',
        ];

        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'Users list');
        View::share('clients',$this->clients);

        return view('admin/users-list');
    }

    function showUser($user_id){
        $user  = User::all()->find($user_id);

        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
            'global/plugins/bower_components/select2-ng/select2.css',
            'global/plugins/bower_components/select2-ng/select2-bootstrap.css',
            'global/plugins/bower_components/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
            'global/plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css',
            'global/plugins/bower_components/x-editable/dist/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css',
            'global/plugins/bower_components/x-editable/dist/inputs-ext/address/address.css',
        ];

        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'admin/css/custom.css',
        ];

        // page level plugins
        $this->js['plugins'] = [
            'global/plugins/bower_components/jquery-mockjax/jquery.mockjax.js',
            'global/plugins/bower_components/moment/min/moment.min.js',
            'global/plugins/bower_components/select2-ng/select2.min.js',
            'global/plugins/bower_components/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
            'global/plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js',
            'global/plugins/bower_components/x-editable/dist/inputs-ext/typeaheadjs/lib/typeahead.js',
            'global/plugins/bower_components/x-editable/dist/inputs-ext/typeaheadjs/typeaheadjs.js',
            'global/plugins/bower_components/x-editable/dist/inputs-ext/address/address.js',
        ];

        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/app/admin.user.form.xeditable.js',
            'admin/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'User: '.$user->name);
        View::share('user', $user);

        return view('admin.user-view');
    }

    function resetPassword($user_id){
        $user  = User::all()->find($user_id);
        $password = str_random(10);
        $user->password = Hash::make($password);
        $user->save();
        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
            'global/plugins/bower_components/select2-ng/select2.css',
            'global/plugins/bower_components/select2-ng/select2-bootstrap.css',
            'global/plugins/bower_components/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
            'global/plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css',
            'global/plugins/bower_components/x-editable/dist/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css',
            'global/plugins/bower_components/x-editable/dist/inputs-ext/address/address.css',
        ];

        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'admin/css/custom.css',
        ];

        // page level plugins
        $this->js['plugins'] = [
            'global/plugins/bower_components/jquery-mockjax/jquery.mockjax.js',
            'global/plugins/bower_components/moment/min/moment.min.js',
            'global/plugins/bower_components/select2-ng/select2.min.js',
            'global/plugins/bower_components/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
            'global/plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js',
            'global/plugins/bower_components/x-editable/dist/inputs-ext/typeaheadjs/lib/typeahead.js',
            'global/plugins/bower_components/x-editable/dist/inputs-ext/typeaheadjs/typeaheadjs.js',
            'global/plugins/bower_components/x-editable/dist/inputs-ext/address/address.js',
        ];

        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/app/admin.user.form.xeditable.js',
            'admin/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'User: '.$user->name);
        View::share('user', $user);
        View::share('password',$password);

        return view('admin.user-view');
    }
    //end of user relating functions
    
    //Start of rule setting relationg functionss
    public function ruleList() {

        $bars = [];

        foreach ($this->clients as $client){
            if(!$client->rules()->isEmpty()){

                $numOfRules = $client->rules()->count();
                $i = 0;
                $rest = 100;
                foreach ($client->rules() as $rule) {
                    $width = ((int)$rule->days) / ((int)($client->daysToCollect()))*100;
                    if (++$i === $numOfRules)
                        $bars[$rule->id . ''] = $rest;
                    else {
                        $rest = $rest - (int)$width;
                        $bars[$rule->id . ''] = (int)$width;
                    }
                }
            }
        }


        $colors = ['progress-bar-primary',
            'progress-bar-success',
            'progress-bar-info',
            'progress-bar-warning',
            'progress-bar-danger',
            'progress-bar-lilac',
            'progress-bar-teals',
            'progress-bar-inverse'
        ];

        //  page level styles
        $this->css['pages'][] = 'global/plugins/bower_components/ion.rangeSlider/css/ion.rangeSlider.css';
        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'admin/css/custom.css',
        ];

        //  page level plugins
        $this->js['plugins'][] = 'global/plugins/bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js';

        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/pages/blankon.slider.js',
            'admin/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'Rule Settings');
        View::share('clients', $this->clients);
        View::share('bars',$bars);
        View::share('colors',$colors);

        return view('admin/rule-list');
    }

    function newRule($client_id){

        $client = Client::where('id',$client_id)->first();

        if($client->rules()->isEmpty()) {
            $rule = new Rule();
            $rule->client_id = $client_id;
            $rule->frequency = 14;
            $rule->days      = 40;
            $rule->save();

            $rule = new Rule();
            $rule->client_id = $client_id;
            $rule->frequency = 7;
            $rule->days      = 40;
            $rule->save();

            $rule = new Rule();
            $rule->client_id = $client_id;
            $rule->frequency = 4;
            $rule->days      = 40;
            $rule->save();
        }

        return $this->editRule($client_id);
    }

    function editRule($client_id)
    {

        $client = Client::where('id',$client_id)->first();
        $numOfRules = $client->rules()->count();
        $i = 0;
        $rest = 100;
        foreach ($client->rules() as $rule) {
            $width = ((int)$rule->days) / ((int)($client->daysToCollect()))*100;
            if (++$i === $numOfRules)
                $bars[$rule->id . ''] = $rest;
            else {
                $rest = $rest - (int)$width;
                $bars[$rule->id . ''] = (int)$width;
            }
        }
        $colors = ['progress-bar-primary',
            'progress-bar-success',
            'progress-bar-info',
            'progress-bar-warning',
            'progress-bar-danger',
            'progress-bar-lilac',
            'progress-bar-teals',
            'progress-bar-inverse'
        ];
        $start = 0;
        //  page level styles
        $this->css['pages'][] = 'global/plugins/bower_components/ion.rangeSlider/css/ion.rangeSlider.css';
        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'admin/css/custom.css',
        ];

        //  page level plugins
        $this->js['plugins'][] = 'global/plugins/bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js';

        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/pages/blankon.slider.js',
            'admin/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'Rule Settings');
        View::share('client', $client);
        View::share('bars',$bars);
        View::share('colors',$colors);

        return view('admin/rule-edit');
    }

    function postEditRule($rule_id){
        $rule = Rule::where('id',$rule_id)->first();
        $rule->days = $_POST['days'];
        $rule->frequency = $_POST['frequency'];
        $rule->sms = $_POST['sms'];
        $rule->email = $_POST['email'];
        if(!$rule->save())
            View::share('error','Failed to save changes.');
        else
            View::share('msg','Save changes successfully.');
        
        return $this->editRule($_POST['client_id']);
    }

    function postAddRule(){
        $rule = New Rule();
        $rule->days = $_POST['days'];
        $rule->client_id = $_POST['client_id'];
        $rule->frequency = $_POST['frequency'];
        $rule->sms = $_POST['sms'];
        $rule->email = $_POST['email'];
        if(!$rule->save())
            View::share('error','Failed to add a new rule.');
        else
            View::share('msg','New rule added.');

        return $this->editRule($_POST['client_id']);
    }

    function deleteRule($rule_id){
        $rule = Rule::where('id',$rule_id)->first();
        if(!$rule->delete())
            View::share('error','Failed to delete a rule.');
        else
            View::share('msg','Rule deleted.');

        return $this->editRule($_GET['client_id']);
    }
    //end of Rules setting related functions
    
    //start of Template setting related functions
    public function templateList() {

        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'admin/css/pages/frontend-themes.css',
            'admin/css/custom.css',
        ];

        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('clients', $this->clients);
        View::share('title', 'EMAIL TEMPLATES LIST');

        return view('admin/template-list');

    }
}
