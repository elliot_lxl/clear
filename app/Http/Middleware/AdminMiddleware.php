<?php

namespace App\Http\Middleware;

use App\ClientUser;
use Closure;
use Auth;
use View;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $group = Auth::user()->group_id;
        if(($group != 2&&$group != 1)||(Auth::user()->clientUser()->isEmpty()))
        {
            $css['themes'] = [
                'admin/css/reset.css',
                'admin/css/layout.css',
                'admin/css/components.css',
                'admin/css/plugins.css',
                'admin/css/themes/laravel.theme.css' => ['id' => 'theme'],
                'admin/css/pages/error-page.css',
                'admin/css/custom.css',
                'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
                'global/plugins/bower_components/animate.css/animate.min.css'
            ];
            $css['globals'] = ['global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css'];
            //  core js
            $js['cores'] = [
                'cores' => [
                    'global/plugins/bower_components/jquery/dist/jquery.min.js',
                    'global/plugins/bower_components/jquery-cookie/jquery.cookie.js',
                    'global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js',
                    'global/plugins/bower_components/typehead.js/dist/handlebars.js',
                    'global/plugins/bower_components/typehead.js/dist/typeahead.bundle.min.js',
                    'global/plugins/bower_components/jquery-nicescroll/jquery.nicescroll.min.js',
                    'global/plugins/bower_components/jquery.sparkline.min/index.js',
                    'global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js',
                    'global/plugins/bower_components/ionsound/js/ion.sound.min.js',
                    'global/plugins/bower_components/bootbox/bootbox.js'
                ],
                'ies' => [
                    'global/plugins/bower_components/html5shiv/dist/html5shiv.min.js',
                    'global/plugins/bower_components/respond-minmax/dest/respond.min.js'
                ]
            ];

            // page level scripts
            $js['scripts'] = [
            ];

            // pass variable to view
            View::share('assetUrl', config('constant.assetUrl'));
            View::share('bodyClass', "page-session page-sound page-header-fixed page-sidebar-fixed");
            View::share('css', $css);
            View::share('js', $js);
            View::share('title', 'No permission.');
            abort(404);
        }

        return $next($request);
    }
}
