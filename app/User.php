<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Symfony\Component\HttpKernel\Client;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password','group_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function clientUser(){
		return ClientUser::where('user_id',$this->id)->where("linked",true)->get();
	}

	public function toClient($client_id){
		return ClientUser::where('user_id',$this->id)->where("client_id",$client_id)->first();
	}

	public function clients(){
		$clients = [];
		foreach ($this->clientUser() as $client_user)
			$clients[] = $client_user->client();
		return $clients;
	}

	public function linkToClient($client_id,$group_id){
		$client_user = new ClientUser();
		$client_user->user_id   = $this->id;
		$client_user->client_id = $client_id;
		$client_user->linked    = true;
		$client_user->admin  = $group_id;
		return $client_user->save();
	}

	public function groupName(){
		$group = UserGroup::all()->find($this->group_id);
		return $group->group_name;
	}
}
