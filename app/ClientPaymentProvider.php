<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientPaymentProvider extends Model
{
    //
    protected $table = 'client_payment_providers';
}
