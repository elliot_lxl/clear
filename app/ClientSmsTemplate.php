<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientSmsTemplate extends Model
{
    //
    protected $table = 'sms_templates';

    public $timestamps = false;

    public function templateContent(){
        return SmsTemplate::where('template_id',$this->id)->first();
    }
}
