<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    //
    protected $table = 'debt_rules';

    public function emailTemplate(){
        return EmailTemplate::where('id',$this->email)->first();
    }

    public function smsTemplates(){
        return EmailTemplate::where('id',$this->email)->first();
    }
}
