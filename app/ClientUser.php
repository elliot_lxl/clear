<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientUser extends Model
{
    //
    protected $table = 'client_users';

    function user(){
        return User::where('id',$this->user_id)->first();
    }

    function client(){
        return Client::where('id',$this->client_id)->first();
    }
}
