@extends('admin.layouts.lay_admin')

        <!-- START @PAGE CONTENT -->
@section('content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2><i class="fa fa-edit"></i> {{$user->name}} <span>updating user information by enable/disable editing</span></h2>
            <div class="breadcrumb-wrapper hidden-xs">
                <span class="label">You are here:</span>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="{{url('dashboard/index')}}">Dashboard</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">User</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">{{$user->name}}</li>
                </ol>
            </div><!-- /.breadcrumb-wrapper -->
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">
                @if(isset($password))
                <div class="row">
                    <div class="col-md-12">
                        <div class="callout callout-info mb-20">
                            <p>New password: <strong class="text-danger">{{$password}}</strong></p>
                            <p>New password has been emailed to <strong>{{$user->email}}</strong> and <strong>{{Auth::user()->email}}</strong>.</p>
                        </div>
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col-md-12">

                        <!-- Start X-Editable -->
                        <div class="panel rounded shadow">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h3 class="panel-title">Click to edit</h3>
                                </div><!-- /.pull-left -->
                                <div class="pull-right">
                                    <button class="btn btn-sm" data-action="refresh" data-toggle="tooltip"
                                            data-placement="top" data-title="Refresh"><i class="fa fa-refresh"></i>
                                    </button>
                                    <button class="btn btn-sm" data-action="collapse" data-toggle="tooltip"
                                            data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i>
                                    </button>
                                    <button class="btn btn-sm" data-action="remove" data-toggle="tooltip"
                                            data-placement="top" data-title="Remove"><i class="fa fa-times"></i>
                                    </button>
                                </div><!-- /.pull-right -->
                                <div class="clearfix"></div>
                            </div><!-- /.panel-heading -->
                            <div class="panel-sub-heading inner-all">
                                <div class="pull-right">
                                    <button id="enable" class="btn btn-primary">enable/disable editing</button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <table id="user" class="table table-bordered table-striped" style="clear: both">
                                    <tbody>
                                    <tr>
                                        <td width="35%">Username</td>
                                        <td width="65%"><a href="#" id="username" name="username" data-type="text" data-pk="1"
                                                           data-title="Enter username">{{$user->name}}</a>
                                            <input type="hidden" name="username1" id="username1" value="{{$user->name}}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Role</td>
                                        <td><a href="#" id="group" name="group" data-type="select" data-pk="1"
                                               data-value="{{$user->gorup_id}}" data-source="/groups"
                                               data-title="Select group">{{$user->groupName()}}</a></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td><a href="#" id="email" name="email" data-type="text" data-pk="1"
                                               data-title="Enter email">{{$user->email}}</a></td>
                                    </tr>
                                    <tr>
                                        <td>Modified on</td>
                                        <td>{{$user->updated_at}}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <button type="submit" class="btn btn-success">submit</button>
                                            <a href="{{url('/client/user/resetpassword/'.$user->id)}}" class="btn btn-warning">Reset Password</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div><!-- /.panel-body -->
                        </div><!-- /.panel -->
                        <!--/ End X-Editable -->

                    </div>
                </div><!-- /.row -->

                {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                        {{--<h3>Console--}}
                            {{--<small>(all ajax requests here are emulated)</small>--}}
                        {{--</h3>--}}
                        {{--<div><textarea id="console" class="form-control" rows="8" autocomplete="off"></textarea></div>--}}
                    {{--</div>--}}
                {{--</div>--}}

        </div><!-- /.body-content -->
        <!--/ End body content -->

        <!-- Start footer content -->
        @include('admin.layouts._footer-admin')
                <!--/ End footer content -->

    </section><!-- /#page-content -->
    @stop
            <!--/ END PAGE CONTENT -->
