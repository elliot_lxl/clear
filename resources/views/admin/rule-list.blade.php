@extends('admin.layouts.lay_admin')

<!-- START @PAGE CONTENT -->
@section('content')
<section id="page-content">

    <!-- Start page header -->
    <div class="header-content">
        <h2><i class="fa fa-sliders"></i> Rule settings</h2>
        <div class="breadcrumb-wrapper hidden-xs">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{url('dashboard/index')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Setting</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li class="active">Rule Settings</li>
            </ol>
        </div><!-- /.breadcrumb-wrapper -->
    </div><!-- /.header-content -->
    <!--/ End page header -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn">

        <div class="row">
            <div class="col-md-12">
                @foreach($clients as $client)
                    <?php $start = 0; ?>
                    @if($client->rules()->isEmpty())
                        <div class="panel rounded shadow">
                            <div class="panel-heading panel-success">
                                <div class="pull-left">
                                    <h3 class="panel-title">{{$client->display_name}}</h3>
                                </div>
                                <div class="pull-right">
                                    <button class="btn btn-sm" data-action="collapse" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                                </div>
                                <div class="clearfix"></div>
                            </div><!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="callout callout-info mb-20">
                                            <p class="h4">No rules has been setup. Now displaying <code class="text-strong">default rules</code>.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-primary hidden-ie" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 33%">
                                        40 days
                                    </div>
                                    <div class="progress-bar progress-bar-warning hidden-ie" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 33%">
                                        40 days
                                    </div>
                                    <div class="progress-bar progress-bar-success hidden-ie" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 34%">
                                        40 days
                                    </div>
                                </div>
                                <div class="well">
                                    <p class="h4">0-40 days Rules</p>
                                    <p>Contact method: <a href="#" class="text-strong">SMS</a> , <a href="#" class="text-strong">EMAIL</a></p>
                                    <p>Frequency: every 14 days</p>
                                </div>
                                <div class="well">
                                    <p class="h4">41-80 days Rules</p>
                                    <p>Contact method: <a href="#" class="text-strong">SMS</a> , <a href="#" class="text-strong">EMAIL</a></p>
                                    <p>Frequency: every 7 days</p>
                                </div>
                                <div class="well">
                                    <p class="h4">81-120 days Rules</p>
                                    <p>Contact method: <a href="#" class="text-strong">SMS</a> , <a href="#" class="text-strong">EMAIL</a></p>
                                    <p>Frequency: every 4 days</p>
                                </div>
                                <a href="{{url('setting/rule/new/'.$client->id)}}" class="btn btn-success">Create your rules</a>
                            </div><!-- /.panel-body -->
                        </div>
                    @else
                        <div class="panel rounded shadow">
                            <div class="panel-heading panel-success">
                                <div class="pull-left">
                                    <h3 class="panel-title">{{$client->display_name}}</h3>
                                </div>
                                <div class="pull-right">
                                    <button class="btn btn-sm" data-action="collapse" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                                </div>
                                <div class="clearfix"></div>
                            </div><!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="progress progress-striped active">
                                    @foreach($client->rules() as $key=>$rule)

                                        <div class="progress-bar {{$colors[$key%count($colors)]}} hidden-ie" role="progressbar"
                                             aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{$bars[$rule->id . '']}}%">
                                            {{$rule->days}} days
                                        </div>

                                    @endforeach
                                </div>
                                @foreach($client->rules() as $rule)
                                    <div class="well">
                                        <p class="h4">{{$start+1}}-{{$start+$rule->days}} days Rules</p>
                                        <p>Contact method:
                                            <a href="#" class="text-strong">SMS</a>
                                            <a href="#" class="text-strong">EMAIL</a>
                                        </p>
                                        <p>Frequency: every {{$rule->frequency}} days</p>
                                    </div>
                                    <?php $start = $start + $rule->days; ?>
                                @endforeach
                                <a href="{{url('setting/rule/edit/'.$client->id)}}" class="btn btn-success">Manage</a>
                            </div><!-- /.panel-body -->
                        </div>
                    @endif
                @endforeach
            </div>
        </div><!-- /.row -->


    </div><!-- /.body-content -->
    <!--/ End body content -->

    <!-- Start footer content -->
    @include('admin.layouts._footer-admin')
    <!--/ End footer content -->

</section><!-- /#page-content -->
@stop
<!--/ END PAGE CONTENT -->
