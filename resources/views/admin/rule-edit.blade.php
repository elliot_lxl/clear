@extends('admin.layouts.lay_admin')

        <!-- START @PAGE CONTENT -->
@section('content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2><i class="fa fa-sliders"></i> Sliders <span>general ui components</span></h2>
            <div class="breadcrumb-wrapper hidden-xs">
                <span class="label">You are here:</span>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="{{url('dashboard/index')}}">Dashboard</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Setting</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Rule Settings</li>
                </ol>
            </div><!-- /.breadcrumb-wrapper -->
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">
            @if(isset($error))
                <div class="row">
                    <div class="alert alert-danger">
                        <strong>{{$error}}</strong>
                    </div>
                </div>
            @elseif(isset($msg))
                <div class="row">
                    <div class="alert alert-success">
                        <strong>{{$msg}}</strong>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="panel rounded shadow">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h3 class="panel-title text-strong">{{$client->display_name}}</h3>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="progress progress-striped active">
                            @foreach($client->rules() as $key=>$rule)

                                <div class="progress-bar {{$colors[$key%count($colors)]}} hidden-ie" role="progressbar"
                                    aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{$bars[$rule->id . '']}}%">
                                    {{$rule->days}} days
                                </div>

                            @endforeach
                            </div>
                            <?php $start = 0; ?>
                            @foreach($client->rules() as $rule)
                                <div class="well">
                                    <p class="h4">{{$start+1}}-{{$start+$rule->days}} days Rules</p>
                                    <p><strong>Contact method</strong>:
                                        @if($rule->sms != 0 )
                                            <a href="#" class="text-strong">SMS</a>
                                        @endif
                                        @if($rule->email != 0 )
                                            <a href="#" class="text-strong">EMAIL</a>
                                        @endif
                                        @if($rule->email == 0 && $rule->sms == 0 )
                                            No contact methods setup. <a href="#" class="text-strong" data-toggle="modal" data-target=".edit-rule-modal-table-{{$rule->id}}"> Click here </a> to setup a contact method.
                                        @endif
                                    </p>
                                    <p><strong>Frequency</strong>: every {{$rule->frequency}} days</p>
                                    <button data-toggle="modal" data-target=".edit-rule-modal-table-{{$rule->id}}" class="btn btn-warning">Edit</button>
                                    <button data-toggle="modal" data-target=".delet-rule-modal-{{$rule->id}}" class="btn btn-danger">delete</button>
                                </div>
                                <?php $start = $start + $rule->days; ?>
                            @endforeach
                            <button data-toggle="modal" data-target=".add-rule-modal-table" class="btn btn-success">Add new rule</button>
                        </div><!-- /.panel-body -->
                    </div>
                </div>
            </div><!-- /.row -->


        </div><!-- /.body-content -->
        <!-- Start table in modal element -->
        <?php $start = 0; ?>
        @foreach($client->rules() as $rule)
            <div class="modal fade edit-rule-modal-table-{{$rule->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h3 class="panel-title">Edit Rule from day {{$start+1}}</h3>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body no-padding">
                                <div class="table-responsive">
                                    <form action="{{url('setting/rule/update/'.$rule->id)}}" method="post" class="form-horizontal form-bordered">
                                        {{csrf_field()}}
                                        <input type="hidden" name="client_id" value="{{$client->id}}">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Days of this stage</label>
                                                <div class="col-sm-9">
                                                    <input class="form-control" type="number" name="days" value="{{$rule->days}}">
                                                </div>
                                            </div><!-- /.form-group -->
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Frequency</label>
                                                <div class="col-sm-9">
                                                    <input class="form-control" type="number" name="frequency" value="{{$rule->frequency}}" placeholder="every {{$rule->frequency}} days">
                                                </div>
                                            </div><!-- /.form-group -->
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Select email template</label>
                                                <div class="col-sm-9">
                                                    <select class="form-control input-lg" name="email">
                                                        <option value="0" @if($rule->email == 0) selected @endif>Disabled</option>
                                                        <option value="1" @if($rule->email == 1) selected @endif>System default templates</option>
                                                        @foreach($client->emailTemplates() as $emailTemplate)
                                                            <option value="{{$emailTemplate->id}}" @if($rule->email == $emailTemplate->id) selected @endif>{{$emailTemplate->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div><!-- /.form-group -->
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Select sms template</label>
                                                <div class="col-sm-9">
                                                    <select class="form-control input-lg" name="sms">
                                                        <option value="0" @if($rule->sms == 0) selected @endif>Disabled</option>
                                                        <option value="1" @if($rule->sms == 1) selected @endif>System default templates</option>
                                                        @foreach($client->smsTemplates() as $smsTemplate)
                                                            <option value="{{$smsTemplate->id}}" @if($rule->sms == $smsTemplate->id) selected @endif>{{$smsTemplate->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div><!-- /.form-group -->
                                        </div><!-- /.form-body -->

                                        <div class="form-footer">
                                            <div class="pull-right">
                                                <button class="btn btn-danger mr-5" data-dismiss="modal">Cancel</button>
                                                <button class="btn btn-success" type="submit">Save changes</button>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div><!-- /.form-footer -->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <div class="modal modal-danger fade delet-rule-modal-{{$rule->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Delete rule</h4>
                        </div>
                        <div class="modal-body">
                            <p>Edit Rule from day {{$start+1}} to day {{$start+$rule->days}}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <a href="{{url('setting/rule/delete/'.$rule->id)}}?client_id={{$client->id}}" class="btn btn-theme">Delete</a>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <?php $start = $start + $rule->days; ?>
        @endforeach
        <div class="modal fade add-rule-modal-table" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h3 class="panel-title">Add New Rule from day {{$start+1}}</h3>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body no-padding">
                            <div class="table-responsive">
                                <form action="{{url('setting/rule/add/')}}" method="post" class="form-horizontal form-bordered">
                                    {{csrf_field()}}
                                    <input type="hidden" name="client_id" value="{{$client->id}}">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Days of this stage</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="number" name="days" value="14">
                                            </div>
                                        </div><!-- /.form-group -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Frequency</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="number" name="frequency" value="3" placeholder="every 3 days">
                                            </div>
                                        </div><!-- /.form-group -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Select email template</label>
                                            <div class="col-sm-9">
                                                <select class="form-control input-lg" name="email">
                                                    <option value="0">Disabled</option>
                                                    <option value="1" selected>System default templates</option>
                                                    @foreach($client->emailTemplates() as $emailTemplate)
                                                        <option value="{{$emailTemplate->id}}">{{$emailTemplate->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div><!-- /.form-group -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Select sms template</label>
                                            <div class="col-sm-9">
                                                <select class="form-control input-lg" name="sms">
                                                    <option value="0">Disabled</option>
                                                    <option value="1" selected>System default templates</option>
                                                    @foreach($client->smsTemplates() as $smsTemplate)
                                                        <option value="{{$smsTemplate->id}}">{{$smsTemplate->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div><!-- /.form-group -->
                                    </div><!-- /.form-body -->

                                    <div class="form-footer">
                                        <div class="pull-right">
                                            <button class="btn btn-danger mr-5" data-dismiss="modal">Cancel</button>
                                            <button class="btn btn-success" type="submit">Save changes</button>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div><!-- /.form-footer -->
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!--/ End tables in modal element -->
        <!--/ End body content -->

        <!-- Start footer content -->
        @include('admin.layouts._footer-admin')
                <!--/ End footer content -->

    </section><!-- /#page-content -->
    @stop
            <!--/ END PAGE CONTENT -->
