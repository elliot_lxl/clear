@extends('admin.layouts.lay_admin')

<!-- START @PAGE CONTENT -->
@section('content')
<section id="page-content">

    <!-- Start page header -->
    <div class="header-content">
        <h2><i class="fa fa-table"></i> Table <span>basic table samples</span></h2>
        <div class="breadcrumb-wrapper hidden-xs">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{url('dashboard/index')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Users</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li class="active">List</li>
            </ol>
        </div><!-- /.breadcrumb-wrapper -->
    </div><!-- /.header-content -->
    <!--/ End page header -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn">

        <div class="row">
            <div class="col-md-12">
                <!-- Start table horizontal scroll -->
                <h4 class="mt-0">Sample Table</h4>
                <div class="table-responsive mb-20">
                    <table class="table table-striped table-success">
                        <thead>
                        <tr>
                            <th>Client</th>
                            <th>Username</th>
                            <th>Role</th>
                            <th class="text-center">Last Login time</th>
                            <th class="text-center">Last Login IP</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($clients as $client)
                            @foreach($client->users() as $user)
                                <tr>
                                    <td>
                                        {{$client->display_name}}
                                    </td>
                                    <td>
                                        <span>{{$user->name}}</span>
                                    </td>
                                    <td>
                                        @if($user->toClient($client->id)->admin)
                                            Administrator
                                        @else
                                            User
                                        @endif
                                    </td>
                                    <td class="text-center">{{$user->last_login_at}}</td>
                                    <td class="text-center">
                                        {{$user->last_login_ip}}
                                    </td>
                                    <td>
                                        @if($user->toClient($client->id)->linked)
                                            enabled
                                        @else
                                            disabled
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a href="{{url('/client/user/view/'.$user->id)}}" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Client</th>
                            <th>Username</th>
                            <th>Role</th>
                            <th class="text-center">Last Login time</th>
                            <th class="text-center">Last Login IP</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div><!-- /.table-responsive -->
                <!--/ End table horizontal scroll -->
            </div>

        </div><!-- /.row -->

    </div><!-- /.body-content -->
    <!--/ End body content -->

    <!-- Start footer content -->
    @include('admin.layouts._footer-admin')
    <!--/ End footer content -->

</section><!-- /#page-content -->
@stop
<!--/ END PAGE CONTENT -->
