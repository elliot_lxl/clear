<!--
START @SIDEBAR LEFT
           |=========================================================================================================================|
           |  TABLE OF CONTENTS (Apply to sidebar left class)                                                                        |
           |=========================================================================================================================|
           |  01. sidebar-box               |  Variant style sidebar left with box icon                                              |
           |  02. sidebar-rounded           |  Variant style sidebar left with rounded icon                                          |
           |  03. sidebar-circle            |  Variant style sidebar left with circle icon                                           |
           |=========================================================================================================================|

-->
<aside id="sidebar-left" class="{{ $sidebarClass or 'sidebar-circle' }}">

    <!-- Start left navigation - profile shortcut -->
    <div class="sidebar-content">
        <div class="media">
            <a class="pull-left has-notif avatar" href="{{url('page/profile')}}">
                <img src="http://img.djavaui.com/?create=50x50,4888E1?f=ffffff" alt="admin">
                <i class="online"></i>
            </a>
            <div class="media-body">
                <h4 class="media-heading">Hello, <span>Lee</span></h4>
                <small>Web Designer</small>
            </div>
        </div>
    </div><!-- /.sidebar-content -->
    <!--/ End left navigation -  profile shortcut -->

    <!-- Start left navigation - menu -->
    <ul class="sidebar-menu">

        <!-- Start navigation - dashboard -->
        <li {!! Request::is('admin') ? 'class="active"' : null !!}>
            <a href="{{url('admin')}}">
                <span class="icon"><i class="fa fa-home"></i></span>
                <span class="text">Dashboard</span>
                {!! Request::is('admin') ? '<span class="selected"></span>' : null !!}
            </a>
        </li>
        <!--/ End navigation - dashboard -->

        <!-- Start category apps -->
        <li class="sidebar-category">
            <span>Collection</span>
            <span class="pull-right"><i class="fa fa-book"></i></span>
        </li>
        <!--/ End category apps -->

        <!-- Start navigation - debts -->
        <li {!! Request::is('debts','debts/*')? 'class="submenu active"' : 'class="submenu"' !!}>
            <a href="javascript:void(0);">
                <span class="icon"><i class="fa fa-globe"></i></span>
                <span class="text">Debts</span>
                <span class="arrow"></span>
                {!! Request::is('debts', 'debts/*') ? '<span class="selected"></span>' : null !!}
            </a>
            <ul>
                <li {!! Request::is('debts','debts/list')? 'class="active"' : null !!}><a href="{{url('debts/list')}}">List</a></li>
                <li {!! Request::is('debts','debts/upload')? 'class="active"' : null !!}><a href="{{url('debts/upload')}}">Upload</a></li>
                <li {!! Request::is('debts','debts/create')? 'class="active"' : null !!}><a href="{{url('debts/create')}}">Create</a></li>
            </ul>
        </li>
        <!--/ End navigation - debts -->

        <!-- Start navigation - payment -->
        <li {!! Request::is('contacts','contact/*')? 'class="submenu active"' : 'class="submenu"' !!}>
            <a href="javascript:void(0);">
                <span class="icon"><i class="fa fa-envelope"></i></span>
                <span class="text">Contacts</span>
                <span class="arrow"></span>
                {!! Request::is('contacts', 'contacts/*') ? '<span class="selected"></span>' : null !!}
            </a>
            <ul>
                <li {!! Request::is('contacts','contacts/list')? 'class="active"' : null !!}><a href="{{url('contacts/list')}}">List</a></li>
                <li {!! Request::is('contacts','contacts/history')? 'class="active"' : null !!}><a href="{{url('contacts/history')}}">History</a></li>
                <li {!! Request::is('contacts','contacts/create')? 'class="active"' : null !!}><a href="{{url('contacts/create')}}">Create</a></li>
            </ul>
        </li>
        <!--/ End navigation - payment -->

        <!-- Start navigation - pages -->
        <li {!! Request::is('payment','payment/*')? 'class="submenu active"' : 'class="submenu"' !!}>
            <a href="javascript:void(0);">
                <span class="icon"><i class="fa fa-file-o"></i></span>
                <span class="text">Payment</span>
                <span class="arrow"></span>
                {!! Request::is('payment', 'payment/*') ? '<span class="selected"></span>' : null !!}
            </a>
            <ul>
                <li {!! Request::is('payment','payment/list')? 'class="active"' : null !!}><a href="{{url('payment/list')}}">List</a></li>
                <li {!! Request::is('payment','payment/history')? 'class="active"' : null !!}><a href="{{url('payment/history')}}">History</a></li>
            </ul>
        </li>
        <!--/ End navigation - pages -->
@if(isset($group)&&$group==2)
        <!-- Start category setting-->
        <li class="sidebar-category">
            <span>Setting</span>
            <span class="pull-right"><i class="fa fa-cog"></i></span>
        </li>
        <!--/ End category setting-->

        <!-- Start navigation - forms -->
        <li {!! Request::is('setting','setting/rule/*')? 'class="submenu active"' : 'class="submenu"' !!}>
            <a href="javascript:void(0);">
                <span class="icon"><i class="fa fa-list-alt"></i></span>
                <span class="text">Collection Rules</span>
                <span class="arrow"></span>
                {!! Request::is('setting', 'setting/rule/*') ? '<span class="selected"></span>' : null !!}
            </a>
            <ul>
                <li {!! Request::is('setting','setting/rule/list')? 'class="active"' : null !!}><a href="{{url('setting/rule/list')}}">Rule Settings</a></li>
                <li {!! Request::is('setting','setting/rule/communication')? 'class="active"' : null !!}><a href="{{url('setting/rule/communication')}}">Communication</a></li>
                <li {!! Request::is('setting','setting/rule/templates')? 'class="active"' : null !!}><a href="{{url('setting/rule/templates')}}">Templates</a></li>
            </ul>
        </li>
        <li {!! Request::is('setting','setting/contact-time')? 'class="submenu active"' : 'class="submenu"' !!}>
            <a href="{{url('setting/contact-time')}}">
                <span class="icon"><i class="fa fa-list-alt"></i></span>
                <span class="text">Contact Time</span>
                {!! Request::is('setting', 'setting/contact-time') ? '<span class="selected"></span>' : null !!}
            </a>
        </li>

        <!--/ End navigation - forms -->


        <!-- Start category Administration -->
        <li class="sidebar-category">
            <span>Admini</span>
            <span class="pull-right"><i class="fa fa-arrows"></i></span>
        </li>
        <!--/ End category Administration -->

        <!-- Start development - client -->
        <li {!! Request::is('client','client/*')? 'class="submenu active"' : 'class="submenu"' !!}>
            <a href="javascript:void(0);">
                <span class="icon"><i class="fa fa-cube"></i></span>
                <span class="text">Client</span>
                <span class="plus"></span>
                {!! Request::is( 'client','client/*') ? '<span class="selected"></span>' : null !!}
            </a>
            <ul>
                <li {!! Request::is('client','client/info')? 'class="active"' : null !!}><a href="{{url('client/info')}}">Information</a></li>
                <li {!! Request::is('client','client/integration')? 'class="active"' : null !!}><a href="{{url('client/integration')}}">Integration</a></li>

            </ul>
        </li>
        <!--/ End development - client -->

        <!-- Start development - user -->
        <li {!! Request::is('client','client/user/*')? 'class="submenu active"' : 'class="submenu"' !!}>
            <a href="javascript:void(0);">
                <span class="icon"><i class="fa fa-list-alt"></i></span>
                <span class="text">User</span>
                <span class="plus"></span>
                {!! Request::is('client','client/user/*') ? '<span class="selected"></span>' : null !!}
            </a>
            <ul>
                <li {!! Request::is('client','client/user/add')? 'class="active"' : null !!}><a href="{{url('client/user/add')}}">Add</a></li>
                <li {!! Request::is('client','client/user/list')? 'class="active"' : null !!}><a href="{{url('client/user/list')}}">list</a></li>
            </ul>
        </li>
        <!--/ End development - user -->
        
@endif
    </ul><!-- /.sidebar-menu -->
    <!--/ End left navigation - menu -->
    <!-- Start left navigation - footer -->
    <div class="sidebar-footer hidden-xs hidden-sm hidden-md">
        <a id="setting" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Setting"><i class="fa fa-cog"></i></a>
        <a id="fullscreen" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Fullscreen"><i class="fa fa-desktop"></i></a>
        <a id="lock-screen" data-url="lock-screen" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Lock Screen"><i class="fa fa-lock"></i></a>
        <a id="logout" data-url="signin" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Logout"><i class="fa fa-power-off"></i></a>
    </div><!-- /.sidebar-footer -->
    <!--/ End left navigation - footer -->

</aside><!-- /#sidebar-left -->
<!--/ END SIDEBAR LEFT -->
