@extends('layouts.lay_account')

        <!-- START @SIGN WRAPPER -->
@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- Start sample validation 1-->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">User signup</h3>
                    </div>
                    <div class="pull-right"></div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body no-padding">
                    <form class="form-horizontal form-bordered" role="form" id="signup_form" action="{{url('/register')}}" method="post">
                        {!! csrf_field() !!}
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Username <span class="asterisk">*</span></label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control input-sm" name="name">
                                </div>
                            </div><!-- /.form-group -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Password <span class="asterisk">*</span></label>
                                <div class="col-sm-7">
                                    <input type="password" class="form-control input-sm" name="password" id="password">
                                </div>
                            </div><!-- /.form-group -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Confirm Password <span class="asterisk">*</span></label>
                                <div class="col-sm-7">
                                    <input type="password" class="form-control input-sm" name="password_confirm">
                                </div>
                            </div><!-- /.form-group -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Email Address <span class="asterisk">*</span></label>
                                <div class="col-sm-7">
                                    <input type="email" class="form-control input-sm" name="email">
                                    <span class="help-block">Just sample email already use : jokowi@jk.co.id, george@bush.gov, bill@gates.com</span>
                                </div>
                            </div><!-- /.form-group -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label">User Group<span class="asterisk">*</span></label>
                                <div class="col-sm-7">
                                    <div class="rdio rdio-theme">
                                        <input id="group_id-admin" type="radio" name="group_id" value="0">
                                        <label for="group_id-admin">Administrator</label>
                                    </div>
                                    <div class="rdio rdio-theme">
                                        <input id="group_id-user" type="radio" name="group_id" value="1">
                                        <label for="group_id-user">User</label>
                                    </div>
                                    <label for="group_id" class="error"></label>
                                    <input type="text" class="hide" id="group_id"/>
                                </div>
                            </div><!-- /.form-group -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Privacy Police <span class="asterisk">*</span></label>
                                <div class="col-sm-7">
                                    <div class="ckbox ckbox-theme">
                                        <input id="terms-of-use-1" type="checkbox" name="terms">
                                        <label for="terms-of-use-1">I have read and accept <a href="#">Terms of Use</a>.</label>
                                    </div>
                                    <label for="terms" class="error"></label>
                                    <input type="text" class="hide" id="terms"/>
                                </div>
                            </div><!-- /.form-group -->
                        </div><!-- /.form-body -->
                        <div class="form-footer">
                            <div class="col-sm-offset-3">
                                <button type="submit" class="btn btn-theme">Sign up</button>
                            </div>
                        </div><!-- /.form-footer -->
                    </form>

                </div>
            </div><!-- /.panel -->
            <!--/ End sample validation 1 -->
            </div>
        </div>
    <script>
        $(document).ready(function () {
            $("#signup_form").validate({
                rules: {
                    name: {
                        required: true,
                        minlength: 2
                    },
                    password: {
                        required: true,
                        minlength: 5
                    },
                    password_confirm: {
                        required: true,
                        minlength: 5,
                        equalTo: "#password"
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    group_id: "required",
                    terms: "required"
                },
                messages: {
                    name: {
                        required: "Enter a username",
                        minlength: jQuery.validator.format("Enter at least {0} characters"),
                        remote: jQuery.validator.format("{0} is already in use")
                    },
                    password: {
                        required: "Provide a password",
                        rangelength: jQuery.validator.format("Enter at least {0} characters")
                    },
                    password_confirm: {
                        required: "Repeat your password",
                        minlength: jQuery.validator.format("Enter at least {0} characters"),
                        equalTo: "Enter the same password as above"
                    },
                    email: {
                        required: "Please enter a valid email address",
                        minlength: "Please enter a valid email address",
                        remote: jQuery.validator.format("{0} is already in use")
                    },
                    group_id: "Choose your user group",
                    terms: "Please check our terms of use again"
                },
                highlight:function(element) {
                    $(element).parents('.form-group').addClass('has-error has-feedback');
                },
                unhighlight: function(element) {
                    $(element).parents('.form-group').removeClass('has-error');
                }
            });
        });

    </script>
@stop
            <!--/ END SIGN WRAPPER -->
