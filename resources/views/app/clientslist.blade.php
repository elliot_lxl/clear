@extends('app.layouts.lay_admin')

        <!-- START @PAGE CONTENT -->
@section('content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2><i class="fa fa-table"></i> Datatable <span>responsive datatable samples</span></h2>
            <div class="breadcrumb-wrapper hidden-xs">
                <span class="label">You are here:</span>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="dashboard.html">Dashboard</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Tables</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Datatable</li>
                </ol>
            </div><!-- /.breadcrumb-wrapper -->
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">

            <div class="row">
                <div class="col-md-12">


                    <!-- Start datatable using ajax -->
                    <div class="panel rounded shadow">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h3 class="panel-title">Employee List <span class="label label-danger">AJAX Support</span></h3>
                            </div>
                            <div class="pull-right">
                                <button class="btn btn-sm" data-action="refresh" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Refresh"><i class="fa fa-refresh"></i></button>
                                <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                                <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.panel-heading -->
                        <div class="panel-body">
                            <!-- Start datatable -->
                            <table id="clients-table" class="table table-striped table-primary">
                                <thead>
                                <tr>
                                    <th data-hide="phone">id</th>
                                    <th data-class="expand">Display Name</th>
                                    <th data-hide="phone">Client Name</th>
                                    <th data-hide="phone">Contact Email</th>
                                    <th data-hide="phone">Time Zone</th>
                                    <th data-hide="phone,tablet">Country</th>
                                    <th data-hide="phone,tablet">Currency</th>
                                </tr>
                                </thead>
                                <!--tbody section is required-->
                                <tbody></tbody>
                                <!--tfoot section is optional-->
                                <tfoot>
                                <tr>
                                    <th>id</th>
                                    <th>Display Name</th>
                                    <th>Client Name</th>
                                    <th>Contact Email</th>
                                    <th>Time Zone</th>
                                    <th>Country</th>
                                    <th>Currency</th>
                                </tr>
                                </tfoot>
                            </table>
                            <!--/ End datatable -->
                        </div><!-- /.panel-body -->
                    </div><!-- /.panel -->
                    <!--/ End datatable using ajax -->



                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->

        </div><!-- /.body-content -->
        <!--/ End body content -->

        <!-- Start footer content -->
        @include('app.layouts._footer-admin')
        <!--/ End footer content -->

    </section><!-- /#page-content -->
    @stop
            <!--/ END PAGE CONTENT -->
