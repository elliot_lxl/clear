@extends('app.layouts.lay_admin')

        <!-- START @PAGE CONTENT -->
@section('content')
    <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
            <h2><i class="fa fa-bars"></i> Tabs & Accordion <span>general ui components</span></h2>
            <div class="breadcrumb-wrapper hidden-xs">
                <span class="label">You are here:</span>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="dashboard.html">Dashboard</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Components</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Tabs & Accordion</li>
                </ol>
            </div><!-- /.breadcrumb-wrapper -->
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn">

            <div class="row">
                <div class="col-md-12">
                    <p class="lead">Add quick, dynamic tab functionality to transition through panes of local content</p>

                </div>

                <div class="col-md-12">

                    <!-- Start vertical tabs with pagination -->
                    <div id="basic-wizard-vertical">
                        <div class="panel panel-tab panel-tab-double panel-tab-vertical row no-margin mb-15 rounded shadow">
                            <!-- Start tabs heading -->
                            <div class="panel-heading no-padding col-lg-3 col-md-3 col-sm-3">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab4-1" data-toggle="tab">
                                            <i class="fa fa-user"></i>
                                            <div>
                                                <span class="text-strong">Step 1</span>
                                                <span>Basic Information</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab4-2" data-toggle="tab">
                                            <i class="fa fa-file-text"></i>
                                            <div>
                                                <span class="text-strong">Step 2</span>
                                                <span>Settings</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab4-3" data-toggle="tab">
                                            <i class="fa fa-credit-card"></i>
                                            <div>
                                                <span class="text-strong">Step 3</span>
                                                <span>Payment Setting</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab4-4" data-toggle="tab">
                                            <i class="fa fa-check-circle"></i>
                                            <div>
                                                <span class="text-strong">Step 4</span>
                                                <span>Contact Setting</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab4-5" data-toggle="tab">
                                            <i class="fa fa-check-circle"></i>
                                            <div>
                                                <span class="text-strong">Step 5</span>
                                                <span>Confirmation</span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div><!-- /.panel-heading -->
                            <!--/ End tabs heading -->

                            <!-- Start tabs content -->
                            <div class="panel-body col-lg-9 col-md-9 col-sm-9">
                                <form class="form-horizontal form-bordered" method="post" action="{{url('app/client/add')}}">
                                    {{csrf_field()}}
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="tab4-1">

                                            <div class="form-body">
                                                <div>
                                                    <h4>Basic information</h4>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Display Name</label>
                                                    <div class="col-sm-9">
                                                        <input class="form-control" type="text" name="display_name">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Legal name</label>
                                                    <div class="col-sm-9">
                                                        <input class="form-control" type="text" name="client_name">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Contact Email</label>
                                                    <div class="col-sm-9">
                                                        <input class="form-control" type="text" name="contact_email_address">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Activated Status</label>
                                                    <div>
                                                        <div class="col-sm-5 rdio rdio-theme">
                                                            <label class="radio-inline">
                                                                <input type="radio" id="radio-online" value="1" name="client_status">
                                                                <label for="radio-online">Online</label>
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input checked="checked" type="radio" id="radio-offline"  value="0" name="client_status">
                                                                <label for="radio-offline">Offline</label>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab4-2">
                                            <div class="form-body">
                                                <div>
                                                    <h4>Product details content</h4>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Select Country</label>
                                                    <div class="col-sm-9">
                                                        <select data-placeholder="Choose a Country" class="chosen-select mb-15" tabindex="-1" style="display: none;" name="country_id">
                                                            @foreach($countries as $country)
                                                                <option value="{{$country->id}}">{{$country->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Select Currency</label>
                                                    <div class="col-sm-9">
                                                        <select data-placeholder="Choose a currency" class="chosen-select mb-15" tabindex="-1" style="display: none;" name="currency_id">
                                                            @foreach($currencies as $currency)
                                                                <option value="{{$currency->id}}">{{$currency->currency}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab4-3">
                                            <div class="form-body">
                                                <div><h4>Payment Setting</h4></div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Payment provider</label>
                                                    <div class="col-sm-9">
                                                        <select data-placeholder="Choose a provider" class="chosen-select mb-15" tabindex="-1" style="display: none;" name="payment_provider_id">
                                                            @foreach($providers as $provider)
                                                                <option value="{{$provider->id}}">{{$provider->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Payment method</label>
                                                    <div class="col-sm-9">
                                                        <select multiple data-placeholder="Select payment methods" class="chosen-select mb-15" name="payment_method_id[]">
                                                            @foreach($methods as $method)
                                                                <option value="{{$method->id}}">{{$method->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab4-4">
                                            <div class="form-body">
                                                <div><h4>Contact Setting</h4></div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Email:</label>
                                                    <div class="col-sm-9">

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">SMS:</label>
                                                    <div class="col-sm-9">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab4-5">
                                            <h4>Confirmation content</h4>
                                            <p>
                                                <button class="btn btn-primary btn-stroke" type="submit">Submit</button>
                                            </p>
                                        </div>

                                    </div>
                                </form>
                            </div><!-- /.panel-body -->
                            <!--/ End tabs content -->
                        </div><!-- /.panel -->
                    </div>
                    <!--/ End vertical tabs with pagination -->

                </div>
            </div><!-- /.row -->



        </div><!-- /.body-content -->
        <!--/ End body content -->

        <!-- Start footer content -->
        @include('app.layouts._footer-admin')
                <!--/ End footer content -->

    </section><!-- /#page-content -->
    @stop
            <!--/ END PAGE CONTENT -->
