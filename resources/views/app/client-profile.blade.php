@extends('app.layouts.lay_admin')

<!-- START @PAGE CONTENT -->
@section('content')
<section id="page-content">

    <!-- Start page header -->
    <div class="header-content">
        <h2><i class="fa fa-male"></i> Profile <span>profile sample</span></h2>
        <div class="breadcrumb-wrapper hidden-xs">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{url('dashboard/index')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Pages</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li class="active">Profile</li>
            </ol>
        </div><!-- /.breadcrumb-wrapper -->
    </div><!-- /.header-content -->
    <!--/ End page header -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn">

        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-4">

                <div class="panel rounded shadow">
                    <div class="panel-body">
                        <div class="inner-all">
                            <ul class="list-unstyled">
                                <li class="text-center">
                                    <img class="img-circle img-bordered-primary" src="http://img.djavaui.com/?create=100x100,4888E1?f=ffffff" alt="Tol Lee">
                                </li>
                                <li class="text-center">
                                    <h4 class="text-capitalize">{{$client->display_name}}</h4>
                                    @if($client->status)
                                        <p class="text-muted text-capitalize"><label class="label label-info">Activated</label></p>
                                    @else
                                        <p class="text-muted text-capitalize"><label class="label label-warning">Unactivated</label></p>
                                    @endif
                                </li>
                                <li>
                                    <button class="btn btn-success text-center btn-block" disabled>PRO Client</button>
                                </li>
                                <li><br/></li>
                                <li>
                                    <div class="btn-group-vertical btn-block">
                                        <a href="" class="btn btn-default"><i class="fa fa-cog pull-right"></i>Edit client</a>
                                        {{--<a href="" class="btn btn-default"><i class="fa fa-sign-out pull-right"></i>Logout</a>--}}
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div><!-- /.panel -->

                <div class="panel panel-theme rounded shadow">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h3 class="panel-title">Contact</h3>
                        </div>
                        {{--<div class="pull-right">--}}
                            {{--<a href="#" class="btn btn-sm btn-success"><i class="fa fa-facebook"></i></a>--}}
                            {{--<a href="#" class="btn btn-sm btn-success"><i class="fa fa-twitter"></i></a>--}}
                            {{--<a href="#" class="btn btn-sm btn-success"><i class="fa fa-google-plus"></i></a>--}}
                        {{--</div>--}}
                        <div class="clearfix"></div>
                    </div><!-- /.panel-heading -->
                    <div class="panel-body no-padding rounded">
                        <ul class="list-group no-margin">
                            <li class="list-group-item"><i class="fa fa-envelope mr-5"></i> {{$client->contact_email_address}}</li>
                            <li class="list-group-item"><i class="fa fa-globe mr-5"></i> {{$client->country()}}</li>
                            {{--<li class="list-group-item"><i class="fa fa-phone mr-5"></i> {{$client->timezone}}</li>--}}
                        </ul>
                    </div><!-- /.panel-body -->
                </div><!-- /.panel -->

            </div>
            <div class="col-lg-9 col-md-9 col-sm-8">
                <div class="profile-cover">
                    <div class="cover rounded shadow no-overflow">
                        <div class="inner-cover">
                            <!-- Start offcanvas btn group menu: This menu will take position at the top of profile cover (mobile only). -->
                            <div class="btn-group cover-menu-mobile hidden-lg hidden-md">
                                <button type="button" class="btn btn-theme btn-sm dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bars"></i>
                                </button>
                                <ul class="dropdown-menu pull-right no-border" role="menu">
                                    <li @if(!isset($_GET['selected'])||$_GET['selected']=='cost')class="active" @endif><a href="{{url('app/client/view/'.$client_id.'?selected=cost')}}"><i class="fa fa-fw fa-clock-o"></i> <span>Cost meters</span></a></li>
                                    <li @if(isset($_GET['selected'])&&$_GET['selected']=='email')class="active" @endif><a href="{{url('app/client/view/'.$client_id.'?selected=email')}}"><i class="fa fa-fw fa-envelope"></i> <span>Emails meters</span>(98)</a></li>
                                    <li @if(isset($_GET['selected'])&&$_GET['selected']=='sms')class="active" @endif><a href="{{url('app/client/view/'.$client_id.'?selected=sms')}}"><i class="fa fa-fw fa-comment"></i> <span>Sms meters</span> <small>(98)</small></a></li>
                                    <li @if(isset($_GET['selected'])&&$_GET['selected']=='invoice')class="active" @endif><a href="{{url('app/client/view/'.$client_id.'?selected=invoice')}}"><i class="fa fa-fw fa-file-text "></i><span> Invoice </span><small>(23)</small></a></li>
                                </ul>
                            </div>
                            <img src="http://img.djavaui.com/?create=760x230,4888E1?f=ffffff" class="img-responsive full-width" alt="cover">
                        </div>
                        <ul class="list-unstyled no-padding hidden-sm hidden-xs cover-menu">
                            <li @if(!isset($_GET['selected'])||$_GET['selected']=='cost')class="active" @endif><a href="{{url('app/client/view/'.$client_id.'?selected=cost')}}"><i class="fa fa-fw fa-clock-o"></i> <span>Cost meters</span></a></li>
                            <li @if(isset($_GET['selected'])&&$_GET['selected']=='email')class="active" @endif><a href="{{url('app/client/view/'.$client_id.'?selected=email')}}"><i class="fa fa-fw fa-envelope"></i> <span>Emails meters</span>(98)</a></li>
                            <li @if(isset($_GET['selected'])&&$_GET['selected']=='sms')class="active" @endif><a href="{{url('app/client/view/'.$client_id.'?selected=sms')}}"><i class="fa fa-fw fa-comment"></i> <span>Sms meters</span> <small>(98)</small></a></li>
                            <li @if(isset($_GET['selected'])&&$_GET['selected']=='invoice')class="active" @endif><a href="{{url('app/client/view/'.$client_id.'?selected=invoice')}}"><i class="fa fa-fw fa-file-text "></i><span> Invoice </span><small>(23)</small></a></li>
                        </ul>
                    </div><!-- /.cover -->

                </div><!-- /.profile-cover -->
                <div class="divider"></div>
                <!--debts list-->
                <div class="panel panel-success rounded shadow">
                    <div class="panel-heading no-border">
                        <div class="pull-left half">
                            <div class="media">
                                <div class="media-body">
                                    <strong>Debts List</strong>
                                </div>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="clearfix"></div>
                    </div><!-- /.panel-heading -->
                    <div class="panel-body no-padding">
                        <table id="datatable-ajax" class="table table-striped table-primary">
                            <thead>
                            <tr>
                                <th data-class="expand">Name</th>
                                <th data-hide="phone">Position</th>
                                <th data-hide="phone">Office</th>
                                <th data-hide="phone">Age</th>
                                <th data-hide="phone,tablet">Start date</th>
                                <th data-hide="phone,tablet">Salary</th>
                            </tr>
                            </thead>
                            <!--tbody section is required-->
                            <tbody></tbody>
                            <!--tfoot section is optional-->
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Position</th>
                                <th>Office</th>
                                <th>Age</th>
                                <th>Start date</th>
                                <th>Salary</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.panel-body -->
                    <div class="panel-footer">
                        <div class="inner-all block">
                        </div><!-- /.inner-all -->
                    </div><!-- /.panel-footer -->
                </div><!-- /.panel -->
                <!--end debts list-->
                <div class="row">
                    <div class="col-md-6">

                        <div class="panel panel-success rounded shadow">
                            <div class="panel-heading no-border">
                                <div class="pull-left half">
                                    <div class="media">
                                        <div class="media-object pull-left">
                                            <i class="fa fa-envelope fa-2x" aria-hidden="true"></i>
                                        </div>
                                        <div class="media-body">
                                            <strong>Email Templates</strong>
                                        </div>
                                    </div>
                                </div><!-- /.pull-left -->

                                <div class="clearfix"></div>
                            </div><!-- /.panel-heading -->
                            <div class="panel-body no-padding">

                                @if(count($client->users())==0)
                                    <div class="line no-margin"></div><!-- /.line -->
                                    <div class="media inner-all no-margin">
                                        <div class="media-body">
                                            <strong class="mb-10">No Template setup</strong>
                                        </div><!-- /.media-body -->
                                    </div><!-- /.media -->
                                @endif

                                @foreach($client->users() as $user)
                                    <div class="line no-margin"></div><!-- /.line -->
                                    <div class="media inner-all no-margin">
                                        <div class="media-body">
                                            <a href="#" class="h4">{{$user->name}}</a>
                                            <p class="block text-muted">
                                                {{$user->email}}
                                            </p>
                                            {{--<em class="text-xs text-muted">Updated on <span class="text-danger">{{$emailTemplate->templateContent()->updated_at}}</span></em>--}}
                                        </div><!-- /.media-body -->
                                    </div><!-- /.media -->
                                @endforeach
                            </div><!-- /.panel-body -->
                            <div class="panel-footer">
                                <div class="inner-all block">
                                    {{count($client->users())}} users. <a href="#">Add New</a>
                                </div><!-- /.inner-all -->
                            </div><!-- /.panel-footer -->
                        </div><!-- /.panel -->
                        <div class="panel panel-success rounded shadow">
                            <div class="panel-heading no-border">
                                <div class="pull-left half">
                                    <div class="media">
                                        <div class="media-object pull-left">
                                            <i class="fa fa-comment fa-2x" aria-hidden="true"></i>
                                        </div>
                                        <div class="media-body">
                                            <strong>Sms Templates</strong>
                                        </div>
                                    </div>
                                </div><!-- /.pull-left -->
                                <div class="clearfix"></div>
                            </div><!-- /.panel-heading -->
                            <div class="panel-body no-padding">

                                @if(($client->smsTemplates()->count())==0)
                                    <div class="line no-margin"></div><!-- /.line -->
                                    <div class="media inner-all no-margin">
                                        <div class="media-body">
                                            <strong class="mb-10">No Template setup</strong>
                                        </div><!-- /.media-body -->
                                    </div><!-- /.media -->
                                @endif

                                @foreach($client->smsTemplates() as $smsTemplate)
                                    <div class="line no-margin"></div><!-- /.line -->
                                    <div class="media inner-all no-margin">
                                        <div class="media-body">
                                            <a href="#" class="h4">{{$smsTemplate->name}}</a>
                                            <p class="block text-muted">
                                                {{substr($smsTemplate->templateContent()->content,0,150)}}
                                            </p>
                                            <em class="text-xs text-muted">Updated on <span class="text-danger">{{$smsTemplate->templateContent()->updated_at}}</span></em>
                                        </div><!-- /.media-body -->
                                    </div><!-- /.media -->
                                @endforeach
                            </div><!-- /.panel-body -->
                            <div class="panel-footer">
                                <div class="inner-all block">
                                    {{$client->smsTemplates()->count()}} templates. <a href="#">Add New</a>
                                </div><!-- /.inner-all -->
                            </div><!-- /.panel-footer -->
                        </div><!-- /.panel -->
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-success rounded shadow">
                            <div class="panel-heading no-border">
                                <div class="pull-left half">
                                    <div class="media">
                                        <div class="media-object pull-left">
                                            <i class="fa fa-history fa-2x" aria-hidden="true"></i>
                                        </div>
                                        <div class="media-body">
                                            <strong>Contact History</strong>
                                        </div>
                                    </div>
                                </div><!-- /.pull-left -->

                                <div class="clearfix"></div>
                            </div><!-- /.panel-heading -->
                            <div class="panel-body no-padding">
                                <div class="line no-margin"></div><!-- /.line -->
                                <div class="media inner-all no-margin">
                                    <div class="media-body">
                                        @if(($client->contactHistory()->count())!=0)
                                            <strong class="mb-10">No contact history setup</strong>
                                        @else
                                            <strong class="mb-10">{{$client->contactHistory()->count()}} records. <a href="#">View all</a></strong>
                                        @endif
                                    </div><!-- /.media-body -->
                                </div><!-- /.media -->
                            </div><!-- /.panel-body -->
                            <div class="panel-footer">
                                <div class="inner-all block">
                                </div><!-- /.inner-all -->
                            </div><!-- /.panel-footer -->
                        </div><!-- /.panel -->
                        <div class="panel panel-success rounded shadow">
                            <div class="panel-heading no-border">
                                <div class="pull-left half">
                                    <div class="media">
                                        <div class="media-object pull-left">
                                            <i class="fa fa-envelope fa-2x" aria-hidden="true"></i>
                                        </div>
                                        <div class="media-body">
                                            <strong>Email Templates</strong>
                                        </div>
                                    </div>
                                </div><!-- /.pull-left -->

                                <div class="clearfix"></div>
                            </div><!-- /.panel-heading -->
                            <div class="panel-body no-padding">

                                @if(count($client->users())==0)
                                    <div class="line no-margin"></div><!-- /.line -->
                                    <div class="media inner-all no-margin">
                                        <div class="media-body">
                                            <strong class="mb-10">No users</strong>
                                        </div><!-- /.media-body -->
                                    </div><!-- /.media -->
                                @endif

                                @foreach($client->users() as $user)
                                    <div class="line no-margin"></div>
                                    <div class="media inner-all no-margin">
                                        <div class="media-body">
                                            <a href="#" class="h4">{{$user->name}}</a>
                                            <p class="block text-muted">
                                                {{$user->email}}
                                            </p>
                                            {{--<em class="text-xs text-muted">Updated on <span class="text-danger">{{$smsTemplate->templateContent()->updated_at}}</span></em>--}}
                                        </div><!-- /.media-body -->
                                    </div><!-- /.media -->
                                    <div class="line no-margin"></div><!-- /.line -->
                                @endforeach
                            </div><!-- /.panel-body -->
                            <div class="panel-footer">
                                <div class="inner-all block">
                                    {{$client->emailTemplates()->count()}} templates. <a href="#">Add New</a>
                                </div><!-- /.inner-all -->
                            </div><!-- /.panel-footer -->
                        </div><!-- /.panel -->
                    </div>
                </div>
            </div>
        </div><!-- /.row -->

    </div><!-- /.body-content -->
    <!--/ End body content -->

    <!-- Start footer content -->
    @include('app.layouts._footer-admin')
    <!--/ End footer content -->

</section><!-- /#page-content -->
@stop
<!--/ END PAGE CONTENT -->
