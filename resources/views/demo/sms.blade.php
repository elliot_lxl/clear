@extends('demo.layouts.lay_admin')

<!-- START @PAGE CONTENT -->
@section('content')
<section id="page-content">

    <!-- Start page div -->
    <div class="header-content">
        <h2><i class="fa fa-pencil"></i> Compose <span>mail compose sample</span></h2>
        <div class="breadcrumb-wrapper hidden-xs">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{url('dashboard/index')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Sms</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li class="active">Send</li>
            </ol>
        </div><!-- /.breadcrumb-wrapper -->
    </div><!-- /.header-content -->
    <!--/ End page div -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn">

        <!-- Start mail page -->
        <div class="row compose-mail-wrapper">

            <div class="col-sm-9">
                <!-- Star form compose mail -->
                <form class="form-horizontal" method="post" action="{{url('portal/sms')}}">
                    {{csrf_field()}}
                    <div class="panel rounded shadow panel-danger">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h3 class="panel-title">View message</h3>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.panel-heading -->
                        <div class="panel-sub-heading inner-all">
                            <div class="pull-left">
                                <ul class="list-inline no-margin">
                                    <li>
                                        <a href="javascript:history.back()" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i> Back</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-send"></i> Send Email</button>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.panel-sub-heading -->
                        <div class="panel-body">
                            <div class="compose-mail">
                                <div class="form-group">
                                    <label for="to" class="col-sm-2 control-label">To:</label>
                                    <div class="col-sm-10">
                                        <div class="input-group  col-sm-12">
                                            <select class="form-control input-sm" name="to" id="to">
                                                <option value="+6581805896">Mark Netto: +65 8180 5896</option>
                                                <option value="+61438110000">Trent: +61 438 110 000</option>
                                                <option value="+61425788917">Elliot: +61 425 788 917</option>
                                            </select>
                                        </div>
                                    </div>
                                </div><!-- /.form-group -->

                                <div class="form-group">
                                    <label for="content" class="col-sm-2 control-label">Content:</label>
                                    <div class="col-sm-10">
                                        <div class="input-group  col-sm-12">
                                        <textarea name="content" id="content" class="form-control" rows="10" placeholder="Write your content here...">Hi Mark, you currently have an outstanding debt of $190.00 with Clear Collection due immediately. Please visit {{url('/payment/hi3785hno')}} to make your payment.</textarea>
                                        </div>
                                    </div>
                                </div><!-- /.form-group -->
                            </div><!-- /.compose-mail -->
                        </div><!-- /.panel-body -->
                        <div class="panel-footer">
                            <div class="pull-right">
                                <button class="btn btn-danger btn-sm">Cancel</button>
                                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-send"></i> Send Email</button>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.panel-footer -->
                    </div><!-- /.panel -->
                </form>
                <!--/ End form compose mail -->
            </div>
        </div><!-- /.row -->
        <!--/ End mail page -->

    </div><!-- /.body-content -->
    <!--/ End body content -->

    <!-- Start footer content -->
    @include('demo.layouts._footer-admin')
    <!--/ End footer content -->

</section><!-- /#page-content -->
@stop
<!--/ END PAGE CONTENT -->
