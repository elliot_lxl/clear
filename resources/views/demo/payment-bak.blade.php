<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Daily UI - 002 - Credit Card Form</title>
    <link rel="stylesheet" href="{{ URL::asset('css/normalize.css') }}">


    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css'>
    <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Roboto:400,700'>
    <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Montserrat'>
    <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Roboto+Mono'>

    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">




</head>

<body>

@if(isset($error_msg))
    <h1><strong>{{$error_msg}}</strong></h1>

@else
<h1>OVER DUE AMOUNT<span>{{$amount}}</span><i class="fa fa-lock"></i></h1>
<div id="container">
    <form>
        <p>
            <label>Name On Card</label>
            <input type="text" placeholder="Super Awesome Customer"/>
        </p>
        <p>
            <label>Card Number</label>
            <input type="text" placeholder="1234 5678 9012 3456"/>
        </p>
        <p>
            <label>Billing Zip Code</label>
            <input type="text" placeholder="12345"/>
        </p>
        <input type="submit" value="Submit Payment"/>
        <input type="button" value="Apply Installment"/>
    </form>
</div>
<h1 class="warning">Please pay before <strong>7th Jun 2016</strong> to avoid penalty.</h1>


@endif

<h1>View <a href="#">Terms and Conditions</a></h1>

</body>
</html>
