@extends('demo.layouts.lay_admin')

<!-- START @PAGE CONTENT -->
@section('content')
<section id="page-content">

    <!-- Start page div -->
    <div class="header-content">
        <h2><i class="fa fa-eye"></i> View <span>mail view sample</span></h2>
        <div class="breadcrumb-wrapper hidden-xs">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{url('dashboard/index')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Contact</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li class="active">SMS</li>
            </ol>
        </div><!-- /.breadcrumb-wrapper -->
        @if(isset($success_msg))<div class="alert alert-info">
            <span class="alert-icon"><i class="fa fa-envelope-o"></i></span>
            <div class="notification-info">
                <ul class="clearfix notification-meta">
                    <li class="pull-left notification-sender"><span>{{$success_msg}}</span></li>
                    <li class="pull-right notification-time">5 seconds ago</li>
                </ul>
            </div>
        </div>
        @elseif(isset($error_msg))
            <div class="alert alert-danger ">
                <span class="alert-icon"><i class="fa fa-comments-o"></i></span>
                <div class="notification-info">
                    <ul class="clearfix notification-meta">
                    <li class="pull-left notification-sender"><span>{{$error_msg}}</span></li>
                    <li class="pull-right notification-time">5 seconds ago</li>
                    </ul>
                </div>
            </div>
        @endif
    </div><!-- /.header-content -->
    <!--/ End page div -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn">

        <!-- Start mail page -->
        <div class="row">
            <div class="col-sm-9">
                <!-- Star form compose mail -->
                <form class="form-horizontal">
                    <div class="panel mail-wrapper rounded shadow">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h3 class="panel-title">Sent SMS</h3>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.panel-heading -->
                        <div class="panel-sub-heading inner-all">
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-xs-7">
                                    <strong>To: </strong>
                                    <span>{{$to}}</span>
                                </div>
                            </div>
                        </div><!-- /.panel-sub-heading -->
                        <div class="panel-body">
                            <div class="view-mail">
                                <strong>Content:</strong>
                                <p>{{$content}}</p>
                            </div><!-- /.view-mail -->
                        </div><!-- /.panel-body -->
                        <div class="panel-footer">
                            <div class="pull-right">
                                <a href="#" class="btn btn-success btn-sm"><i class="fa fa-reply"></i> View Debt Detail</a>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.panel-footer -->
                    </div><!-- /.panel -->
                </form>
                <!--/ End form compose mail -->
            </div>
        </div><!-- /.row -->
        <!--/ End mail page -->

    </div><!-- /.body-content -->
    <!--/ End body content -->

    <!-- Start footer content -->
    @include('demo.layouts._footer-admin')
    <!--/ End footer content -->

</section><!-- /#page-content -->
@stop
<!--/ END PAGE CONTENT -->
