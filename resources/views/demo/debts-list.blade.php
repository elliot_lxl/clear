@extends('demo.layouts.lay_admin')

<!-- START @PAGE CONTENT -->
@section('content')
<section id="page-content">

    <!-- Start page header -->
    <div class="header-content">
        <h2><i class="fa fa-file-text"></i> Debts List <span>View all the debts.</span></h2>
        <div class="breadcrumb-wrapper hidden-xs">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{url('dashboard/index')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Debts</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li class="active">List</li>
            </ol>
        </div><!-- /.breadcrumb-wrapper -->
    </div><!-- /.header-content -->
    <!--/ End page header -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn">

        <!--

        Start blog list
        |=========================================================================================================================|
        |  TABLE OF CONTENTS                                                                               |
        |=========================================================================================================================|
        |  01. blog-grid                |  Variant style blog post type grid                                                      |
        |  02. blog-list                |  Variant style blog post type list                                                      |
        |=========================================================================================================================|

        -->

        <div id="blog-list">
            <div class="row">
                <div class="col-md-12">
                    <div class="blog-item blog-quote rounded shadow">
                        <div class="quote quote-success">
                            <a href="#">
                                <p class="h3 text-justify">Collected: <strong>$181.00</strong></p>
                                <small class="quote-author text-justify">Collect  On: <strong>May 30, 2016</strong></small>
                            </a>
                        </div>
                        <div class="blog-details">
                            <ul class="blog-meta">
                                <li>Debtor: <a href="http://djavaui.com/" target="_blank">John Smith</a></li>
                                <li>Due Date: Jun 80, 2016</li>
                            </ul>
                            <ul class="blog-meta">
                                <li><a href="#">1 Email</a> send</li>
                                <li><a href="#">1 SMS</a> send</li>
                                <li>Viewed <a href="#">5 times</a></li>
                            </ul>
                        </div><!-- blog-details -->
                    </div><!-- /.blog-item -->
                    <div class="blog-item blog-quote rounded shadow">
                        <div class="quote quote-danger">
                            <a href="#">
                                <p class="h3 text-justify">Overdue: <strong>$181.00</strong></p>
                                <small class="quote-author text-justify">Due Date: <strong>Jun 19, 2016</strong></small>
                            </a>
                        </div>
                        <div class="blog-details">
                            <ul class="blog-meta">
                                <li>Debtor: <a href="http://djavaui.com/" target="_blank">Clear collect PTE</a></li>
                            </ul>
                            <ul class="blog-meta">
                                <li><a href="#">4 Email</a> send</li>
                                <li><a href="#">0 SMS</a> send</li>
                                <li>Viewed <a href="#">0 times</a></li>
                            </ul>
                            <a href="{{url('portal/sms')}}" class="btn btn-success">Send Sms</a>
                            <a href="#" class="btn btn-success">Send Email</a>
                        </div><!-- blog-details -->
                    </div><!-- /.blog-item -->
                    <div class="blog-item blog-quote rounded shadow">
                        <div class="quote quote-lilac">
                            <a href="#">
                                <p class="h3 text-justify">Overdue: <strong>$359.15</strong></p>
                                <p class="h3 text-justify">Paid: <strong>$181.55</strong></p>

                                <small class="quote-author text-justify">Next payment on Jun 1, 2016</small>
                            </a>
                        </div>
                        <div class="blog-details">
                            <ul class="blog-meta">
                                <li>Debtor: <a href="http://djavaui.com/" target="_blank">Clear collect PTE</a></li>
                            </ul>
                            <ul class="blog-meta">
                                <li><a href="#">4 Email</a> send</li>
                                <li><a href="#">0 SMS</a> send</li>
                                <li>Viewed <a href="#">0 times</a></li>
                            </ul>
                            <a href="#" class="btn btn-success">Send Sms</a>
                            <a href="#" class="btn btn-success">Send Email</a>

                            <div class="ribbon-wrapper">
                                <div class="ribbon ribbon-success">Installment</div>
                            </div>
                        </div><!-- blog-details -->
                    </div><!-- /.blog-item -->
                    <div class="blog-item blog-quote rounded shadow">
                        <div class="quote quote-teal">
                            <a href="#">
                                <p class="h3 text-justify">Voided: <strong>$359.15</strong></p>

                                <small class="quote-author text-justify">Voided on Jun 1, 2016</small>
                            </a>
                        </div>
                        <div class="blog-details">
                            <ul class="blog-meta">
                                <li>Debtor: <a href="http://djavaui.com/" target="_blank">Clear collect PTE</a></li>
                            </ul>
                            <ul class="blog-meta">
                                <li><a href="#">0 Email</a> send</li>
                                <li><a href="#">0 SMS</a> send</li>
                                <li>Viewed <a href="#">0 times</a></li>
                            </ul>
                            <div class="blog-summary">
                                <p>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat...</p>
                                <a href="#l" class="btn btn-sm btn-success">Read More</a>
                            </div>
                        </div><!-- blog-details -->
                    </div><!-- /.blog-item -->
                    <div class="blog-item blog-quote rounded shadow">
                        <div class="quote quote-danger">
                            <a href="#">
                                <p class="h3 text-justify">Overdue: <strong>$359.15</strong></p>
                                <small class="quote-author text-justify">Due Date: Jun 1, 2016</small>
                            </a>
                        </div>
                        <div class="blog-details">

                            <ul class="blog-meta">
                                <li>Debtor: <a href="http://djavaui.com/" target="_blank">Williams</a></li>
                            </ul>
                            <ul class="blog-meta">
                                <li><a href="#">12 Email</a> send</li>
                                <li><a href="#">6 SMS</a> send</li>
                                <li>Viewed <a href="#">0 times</a></li>
                            </ul>
                            <div class="blog-summary">
                                <p><strong>Note:</strong> Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias ...</p>
                                <a href="#" class="btn btn-sm btn-success">Read More</a>
                            </div>
                        </div><!-- blog-details -->
                    </div><!-- /.blog-item -->
                </div>
            </div>
        </div><!-- /#blog-list -->

        <!--/ End blog-list -->

        <p class="text-center"><img src="{{$assetUrl}}global/img/loader/general/2.gif" alt="..."/> Load more post</p>

    </div><!-- /.body-content -->
    <!--/ End body content -->

    <!-- Start footer content -->
    @include('demo.layouts._footer-admin')
    <!--/ End footer content -->

</section><!-- /#page-content -->
@stop
<!--/ END PAGE CONTENT -->
