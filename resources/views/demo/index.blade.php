@extends('demo.layouts.lay_admin')

<!-- START @PAGE CONTENT -->
@section('content')
<section id="page-content">

    <!-- Start page header -->
    <div class="header-content">
        <h2><i class="fa fa-home"></i>Dashboard <span>dashboard & statistics</span></h2>
        <div class="breadcrumb-wrapper hidden-xs">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li class="active">Dashboard</li>
            </ol>
        </div>
    </div><!-- /.header-content -->
    <!--/ End page header -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn">

        <div class="row">
            <div class="col-md-9">

                <!-- Start widget visitor chart -->
                <div class="panel stat-stack widget-visitor rounded shadow">
                    <div class="panel-body no-padding br-3">
                        <div class="row row-merge">
                            <div class="col-sm-8">
                                <div class="panel panel-theme stat-left no-margin no-box-shadow">
                                    <div class="panel-heading no-border">
                                        <div class="pull-left">
                                            <h3 class="panel-title">Daily Visitor</h3>
                                        </div><!-- /.pull-left -->
                                        <div class="pull-right">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-theme dropdown-toggle no-border" data-toggle="dropdown">
                                                    Duration <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right no-border">
                                                    <li class="dropdown-header">Select duration :</li>
                                                    <li><a href="#">Year</a></li>
                                                    <li><a href="#">Month</a></li>
                                                    <li><a href="#">Week</a></li>
                                                    <li><a href="#">Day</a></li>
                                                </ul>
                                            </div>
                                        </div><!-- /.pull-right -->
                                        <div class="clearfix"></div>
                                    </div><!-- /.panel-heading -->
                                    <div class="panel-body bg-theme">

                                        <div id="contact-chart" class="resize-chart"></div>

                                    </div><!-- /.panel-body -->
                                    <div class="panel-footer no-border-top">
                                        <div class="row text-center">
                                            <div class="col-xs-4 col-xs-override border-right dotted">
                                                <p class="text-success text-strong mb-0">+ 5%</p>
                                                <p class="h3 text-strong mb-0 mt-10 counter-visit" data-counter="7341">7,341</p>
                                                <p class="text-muted">Payment Made</p>
                                            </div>
                                            <div class="col-xs-4 col-xs-override border-right dotted">
                                                <p class="text-success text-strong mb-0">+ 32%</p>
                                                <p class="h3 text-strong mb-0 mt-10 counter-unique" data-counter="23762">23,762</p>
                                                <p class="text-muted">Payment Page Viewed</p>
                                            </div>
                                            <div class="col-xs-4 col-xs-override">
                                                <p class="text-success text-strong mb-0">+ 76%</p>
                                                <p class="h3 text-strong mb-0 mt-10 counter-page" data-counter="70112">70,112</p>
                                                <p class="text-muted">Contact Made</p>
                                            </div>
                                        </div>
                                    </div><!-- /.panel-footer -->
                                </div><!-- /.panel -->
                            </div><!-- /.col-sm-8 -->
                            <div class="col-sm-4">
                                <div class="panel stat-right no-margin no-box-shadow">
                                    <div class="panel-body">
                                        <h4 class="no-margin">Monthly Cost</h4>
                                        <p class="text-muted">Summary of usage</p>

                                        <span>Users</span><span class="pull-right">(5/10)</span>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar progress-bar-lilac" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%"></div>
                                        </div><!-- /.progress -->

                                        <span>SMS</span><span class="pull-right">(121/1000)</span>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar progress-bar-teal" role="progressbar" aria-valuenow="12" aria-valuemin="0" aria-valuemax="100" style="width: 12.1%"></div>
                                        </div><!-- /.progress -->

                                        <span>Emails</span><span class="pull-right">(3000/3000)</span>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                        </div><!-- /.progress -->

                                        <span>Extra Sms: 0</span><span class="pull-right">(10 cents/sms)</span>
                                        <div class="text-teal">$0.00</div>

                                        <span>Extra Emails: 181</span><span class="pull-right">(5 cents/sms)</span>
                                        <div class="text-teal">$9.05</div>

                                        <span>Monthly charge</span>
                                        <div class="text-teal">$199.00</div>
                                    </div><!-- /.panel-body -->
                                    <div class="panel-footer">
                                        <br>
                                        <div>
                                            <blockquote>
                                                Total cost:
                                                <p class="text-warning">$208.05</p>
                                            </blockquote>
                                        </div>
                                    </div>
                                </div><!-- /.panel -->
                            </div><!-- /.col-sm-4 -->
                        </div><!-- /.row -->
                    </div><!-- /.panel-body -->
                </div><!-- /.panel -->
                <!--/ End widget visitor chart -->

            </div>
            <div class="col-md-3">

                <div class="row">

                    <div class="col-lg-12 col-md-12 col-sm-8 col-xs-12">

                        <!-- Start blog post widget -->
                        <div class="blog-item blog-quote rounded shadow">
                            <div class="quote quote-danger">
                                <a href="#">
                                   <i class="fa fa-exclamation-triangle fa-2x" aria-hidden="true"></i>
                                   <Strong class="h4 text_bigger">Overdue debts: 153</Strong>
                                </a>
                            </div>
                            <div class="blog-details">
                                <ul class="blog-meta">
                                    <li>Total: <a href="#">$3217.85</a> waiting for payment</li>
                                </ul>
                            </div><!-- blog-details -->
                        </div><!-- blog-item -->
                        <!--/ End blog post widget -->

                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-8 col-xs-12">

                        <!-- Start blog post widget -->
                        <div class="blog-item blog-quote rounded shadow">
                            <div class="quote quote-success">
                                <a href="#">
                                    <i class="fa fa-check fa-2x" aria-hidden="true"></i>
                                    <Strong class="h4 text_bigger">Collected debts: 268</Strong>
                                </a>
                            </div>
                            <div class="blog-details">
                                <ul class="blog-meta">
                                    <li>Total: <a href="#">$5857.95</a> recived</li>
                                </ul>
                            </div><!-- blog-details -->
                        </div><!-- blog-item -->
                        <!--/ End blog post widget -->

                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-8 col-xs-12">

                        <!-- Start blog post widget -->
                        <div class="blog-item blog-quote rounded shadow">
                            <div class="quote quote-lilac">
                                <a href="#">
                                    <i class="fa fa-circle-o-notch fa-2x" aria-hidden="true"></i>
                                    <Strong class="h4 text_bigger">Installment: 122</Strong>
                                </a>
                            </div>
                            <div class="blog-details">
                                <div class="blog-details">
                                    <ul class="blog-meta">
                                        <li>Total: <a href="#">$757.95</a> received and $2921.00 further payment</li>
                                    </ul>
                                </div>
                            </div><!-- blog-details -->
                        </div><!-- blog-item -->
                        <!--/ End blog post widget -->

                    </div>
                </div>

            </div>
        </div><!-- /.row -->
        <div class="row">
            <div class="col-md-9">

                <!-- Start sample table -->
                <div class="table-responsive rounded mb-20">
                    <table class="table table-striped table-theme">
                        <thead>
                        <tr>
                            <th class="text-center border-right">No.</th>
                            <th>Name</th>
                            <th>Location</th>
                            <th>Position</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-center border-right">1</td>
                            <td>
                                <img src="http://img.djavaui.com/?create=35x35,81B71A?f=ffffff" class="img-circle img-bordered-theme" alt="John Kribo">
                                <span>John Kribo</span>
                            </td>
                            <td>United State</td>
                            <td>Senior Web Designer</td>
                            <td class="text-center">
                                <a href="#" class="btn btn-success btn-xs rounded" data-toggle="tooltip" data-placement="top" data-original-title="View detail"><i class="fa fa-eye"></i></a>
                                <a href="#" class="btn btn-primary btn-xs rounded" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="#" class="btn btn-danger btn-xs rounded" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center border-right">2</td>
                            <td>
                                <img src="http://img.djavaui.com/?create=35x35,A90329?f=ffffff" class="img-circle img-bordered-theme" alt="Jennifer Poiyem">
                                <span>Jennifer Poiyem</span>
                            </td>
                            <td>Spain</td>
                            <td>Senior UX Designer</td>
                            <td class="text-center">
                                <a href="#" class="btn btn-success btn-xs rounded" data-toggle="tooltip" data-placement="top" data-original-title="View detail"><i class="fa fa-eye"></i></a>
                                <a href="#" class="btn btn-primary btn-xs rounded" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="#" class="btn btn-danger btn-xs rounded" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center border-right">3</td>
                            <td>
                                <img src="http://img.djavaui.com/?create=35x35,F4645F?f=ffffff" class="img-circle img-bordered-theme" alt="Clara Wati">
                                <span>Clara Wati</span>
                            </td>
                            <td>United State</td>
                            <td>Developer</td>
                            <td class="text-center">
                                <a href="#" class="btn btn-success btn-xs rounded" data-toggle="tooltip" data-placement="top" data-original-title="View detail"><i class="fa fa-eye"></i></a>
                                <a href="#" class="btn btn-primary btn-xs rounded" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="#" class="btn btn-danger btn-xs rounded" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center border-right">4</td>
                            <td>
                                <img src="http://img.djavaui.com/?create=35x35,6880B0?f=ffffff" class="img-circle img-bordered-theme" alt="Toni Mriang">
                                <span>Toni Mriang</span>
                            </td>
                            <td>Germany</td>
                            <td>Assistant</td>
                            <td class="text-center">
                                <a href="#" class="btn btn-success btn-xs rounded" data-toggle="tooltip" data-placement="top" data-original-title="View detail"><i class="fa fa-eye"></i></a>
                                <a href="#" class="btn btn-primary btn-xs rounded" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="#" class="btn btn-danger btn-xs rounded" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center border-right">5</td>
                            <td>
                                <img src="http://img.djavaui.com/?create=35x35,5a67a5?f=ffffff" class="img-circle img-bordered-theme" alt="Bella negoro">
                                <span>Bella negoro</span>
                            </td>
                            <td>England</td>
                            <td>Developer</td>
                            <td class="text-center">
                                <a href="#" class="btn btn-success btn-xs rounded" data-toggle="tooltip" data-placement="top" data-original-title="View detail"><i class="fa fa-eye"></i></a>
                                <a href="#" class="btn btn-primary btn-xs rounded" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="#" class="btn btn-danger btn-xs rounded" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th class="text-center border-right">No.</th>
                            <th>Name</th>
                            <th>Location</th>
                            <th>Position</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div><!-- /.table-responsive -->
                <!--/ End sample table -->



            </div>
            <div class="col-md-3">

                <!-- Start mini stats social widget -->
                <div class="row">
                    <div class="col-md-12  col-xs-4 col-xs-override">

                        <div class="panel rounded shadow">
                            <div class="panel-heading text-center bg-youtube">
                                <p class="inner-all no-margin">
                                    <i class="fa fa-envelope-o fa-4x"></i>
                                </p>
                            </div><!-- /.panel-heading -->
                            <div class="panel-body text-center">
                                <p class="h5 no-margin inner-all text-strong">
                                    <span class="block">342</span>
                                    <span class="block">Contacts</span>
                                </p>
                            </div><!-- /.panel-body -->
                        </div><!-- /.panel -->

                    </div>
                    <div class="col-md-12  col-xs-4 col-xs-override">

                        <div class="panel rounded shadow">
                            <div class="panel-heading text-center bg-soundcloud">
                                <p class="inner-all no-margin">
                                    <i class="fa fa-folder-o fa-4x"></i>
                                </p>
                            </div><!-- /.panel-heading -->
                            <div class="panel-body text-center">
                                <p class="h5 no-margin inner-all text-strong">
                                    <span class="block">6</span>
                                    <span class="block">Templates</span>
                                </p>
                            </div><!-- /.panel-body -->
                        </div><!-- /.panel -->

                    </div>
                    <div class="col-md-12  col-xs-4 col-xs-override">

                        <div class="panel rounded shadow">
                            <div class="panel-heading text-center bg-dribbble">
                                <p class="inner-all no-margin">
                                    <i class="fa fa-cogs fa-4x"></i>
                                </p>
                            </div><!-- /.panel-heading -->
                            <div class="panel-body text-center">
                                <p class="h5 no-margin inner-all text-strong">
                                    <span class="block">15</span>
                                    <span class="block">Reminder settings</span>
                                </p>
                            </div><!-- /.panel-body -->
                        </div><!-- /.panel -->

                    </div>
                </div>

                <!--/ End mini stats social widget -->

            </div>
        </div><!-- /.row -->

    </div><!-- /.body-content -->
    <!--/ End body content -->

    <!-- Start footer content -->
    @include('demo.layouts._footer-admin')
    <!--/ End footer content -->

</section><!-- /#page-content -->
@stop
<!--/ END PAGE CONTENT -->
