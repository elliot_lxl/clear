<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{$title}}</title>
    <!-- START @FONT STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Oswald:700,400" rel="stylesheet">
    <!--/ END FONT STYLES -->
    <!-- START @GLOBAL MANDATORY STYLES -->
    @if(!empty($css['globals']))
        @foreach($css['globals'] as $global)
            <link href="{{$assetUrl.$global}}" rel="stylesheet">
            @endforeach
            @endif
                    <!--/ END GLOBAL MANDATORY STYLES -->
            <!--/ END GLOBAL MANDATORY STYLES -->

            <!-- START @PAGE LEVEL STYLES -->
            @if(!empty($css['pages']))
                @foreach($css['pages'] as $page)
                    <link href="{{$assetUrl.$page}}" rel="stylesheet">
                    @endforeach
                    @endif
                            <!--/ END PAGE LEVEL STYLES -->

                    <!-- START @THEME STYLES -->
                    @if(!empty($css['themes']))
                        @foreach($css['themes'] as $key=>$theme)
                            @if(is_array($theme))
                                <link href="{{$assetUrl.$key}}" rel="stylesheet" id="{{$theme['id']}}">
                            @else
                                <link href="{{$assetUrl.$theme}}" rel="stylesheet">
                                @endif

                                @endforeach
                                @endif
                                        <!--/ END THEME STYLES -->

                                <!-- START @IE SUPPORT -->
                                <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
                                <!--[if lt IE 9]>
                                @if(!empty($js['ies']))
                                @foreach($js['ies'] as $ie)
                                <script src="{{$assetUrl.$ie}}"></script>
                                @endforeach
                                @endif
                                <![endif]-->
                                <!--/ END IE SUPPORT -->

</head>
<body style="background: white">
<!-- START @ALL MODALS -->
@if((Request::is('component','component/modals')))
@include('component._all-modals')
@endif
        <!--/ END ALL MODALS -->


<!--/ END WRAPPER -->




<!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
<!-- START @CORE PLUGINS -->
@if(!empty($js['cores']))
    @foreach($js['cores'] as $core)
        <script src="{{$assetUrl.$core}}"></script>
        @endforeach
        @endif
                <!--/ END CORE PLUGINS -->

        <!-- START @PAGE LEVEL PLUGINS -->

        @if(!empty($js['additionalScripts']))
            @foreach($js['additionalScripts'] as $script)
                <script src="{{$script}}"></script>
            @endforeach
        @endif

        @if(!empty($js['plugins']))
            @foreach($js['plugins'] as $plugin)
                <script src="{{$assetUrl.$plugin}}"></script>
                @endforeach
                @endif
                        <!--/ END PAGE LEVEL PLUGINS -->

                <!-- START @PAGE LEVEL SCRIPTS -->
                @if(!empty($js['scripts']))
                    @foreach($js['scripts'] as $script)
                        <script src="{{$assetUrl.$script}}"></script>
                        @endforeach
                        @endif

                                <!--/ END PAGE LEVEL SCRIPTS -->
                        <!--/ END JAVASCRIPT SECTION -->

    <!-- start of content -->
                        <!-- Start page div -->


                        <div class="wrapper">



                            <div class="container">

                                <div class="row">

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="panel rounded shadow" style="border: none;">
                                            <div class="panel-heading"><img src="http://i.stack.imgur.com/FBVKH.jpg" height="150"></div>
                                            <div class="panel-body" style="background: #EAEAEA;">

                                                <blockquote class="hero">


                                                            @if(isset($error_msg))
                                                            <center>
                                                                <p class="text-danger">{{$error_msg}}</p>
                                                            </center>
                                                            @else
                                                                <h2 class="blog-title">Hi Mark,</h2>
                                                                <p class="h3">
                                                                    You currently have an outstanding balance of
                                                                </p>
                                                                <br>

                                                                <center>
                                                                    <p class="h1">$192.10</p>
                                                                    <p><a href="#" class="btn btn-danger btn-stroke">Pay now</a></p>
                                                                    <p><a href="#" class="success-color">Terms and conditions</a></p>
                                                                </center>
                                                            @endif


                                                </blockquote>
                                            </div><!-- panel-body -->
                                        </div><!-- panel-blog -->
                                    </div>
                                </div>
                                <div class="row">

                                        <div class="col-sm-12 col-md-12 col-lg-6">

                                            <div class="panel rounded shadow" >
                                                <div class="panel-body bg-googleplus">
                                                    <ul class="inner-all list-unstyled">
                                                        @if(isset($error_msg))
                                                            <li class="h3 text-center">Legal Documents</li>
                                                            <li><br></li>
                                                        @else
                                                            <li class="h3 text-center">33 Days</li>
                                                            <li class="text-center">to Legal Action</li>
                                                            <li><br></li>
                                                        @endif
                                                        <li class="text-center"><a href="#" class="btn btn-lg btn-default active">View details</a></li>
                                                    </ul>
                                                </div>
                                            </div><!-- panel-blog -->
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-6">
                                            <div class="panel rounded shadow" >
                                                <div class="panel-body bg-twitter">
                                                    <ul class="inner-all list-unstyled">
                                                            @if(isset($error_msg))
                                                                <li class="h3 text-center">Flexible payment options.</li>
                                                                <li><br></li>
                                                                <li class="text-center"><a href="#" class="btn btn-lg btn-default active">Peak inside</a></li>
                                                            @else
                                                                <li class="h3 text-center">You are eligible to make installment.</li>
                                                                <li><br></li>
                                                                <li><br></li>
                                                                <li class="text-center"><a href="#" class="btn btn-lg btn-default active">Apply now</a></li>
                                                            @endif
                                                    </ul><!-- panel-blog -->
                                                </div><!-- panel-body -->
                                            </div><!-- panel-blog -->
                                        </div>

                                </div>
                            </div>

                        </div>





    <!-- end of content -->
</body>
</html>