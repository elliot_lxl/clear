'use strict';
var ClientsTable = function () {

    // =========================================================================
    // SETTINGS APP
    // =========================================================================

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            ClientsTable.datatable();
            ClientsTable.highlightRow();
            ClientsTable.clickRow();
        },

        // =========================================================================
        // DATATABLE
        // =========================================================================
        datatable: function () {
            var responsiveHelperAjax = undefined;
            var breakpointDefinition = {
                tablet: 1024,
                phone : 480
            };

            var tableAjax = $('#users-table');

            // Using AJAX
            tableAjax.dataTable({
                autoWidth      : false,
                ajax           : {
                    url:BlankonApp.handleBaseURL() + '/app/user/users',
                    dataSrc:''
                },
                columns: [
                    {data: 'id'},
                    {data: 'name'},
                    {data: 'email'},
                    {data: 'group'},
                    {data: 'updated_at'},
                ],
                preDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelperAjax) {
                        responsiveHelperAjax = new ResponsiveDatatablesHelper(tableAjax, breakpointDefinition);
                    }
                },
                rowCallback    : function (nRow) {
                    responsiveHelperAjax.createExpandIcon(nRow);
                },
                drawCallback   : function (oSettings) {
                    responsiveHelperAjax.respond();
                }
            });



        },
        highlightRow:function () {

        },
        clickRow:function () {
            var tableAjax = $('#users-table').DataTable();
            var tableBody = $('#users-table tbody');
            tableBody.on('click', 'tr', function () {
                var data = tableAjax.row( this ).data();
                window.location.href = BlankonApp.handleBaseURL() + 'app/user/' + data['id'] ;
            } );
        }

    };

}();

// Call main app init
ClientsTable.init();