'use strict';
var ClientsTable = function () {

    // =========================================================================
    // SETTINGS APP
    // =========================================================================

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            ClientsTable.datatable();
            ClientsTable.highlightRow();
            ClientsTable.clickRow();
        },

        // =========================================================================
        // DATATABLE
        // =========================================================================
        datatable: function () {
            var responsiveHelperAjax = undefined;
            var breakpointDefinition = {
                tablet: 1024,
                phone : 480
            };

            var tableAjax = $('#clients-table');

            // Using AJAX
            tableAjax.dataTable({
                autoWidth      : false,
                ajax           : {
                    url:BlankonApp.handleBaseURL() + '/app/client/clients',
                    dataSrc:''
                },
                columns: [
                    {data: 'id'},
                    {data: 'display_name'},
                    {data: 'client_name'},
                    {data: 'contact_email_address'},
                    {data: 'timezone'},
                    {data: 'country_name'},
                    {data: 'currency_name'}
                ],
                preDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelperAjax) {
                        responsiveHelperAjax = new ResponsiveDatatablesHelper(tableAjax, breakpointDefinition);
                    }
                },
                rowCallback    : function (nRow) {
                    responsiveHelperAjax.createExpandIcon(nRow);
                },
                drawCallback   : function (oSettings) {
                    responsiveHelperAjax.respond();
                }
            });



        },
        highlightRow:function () {

        },
        clickRow:function () {
            var tableAjax = $('#clients-table').DataTable();
            var tableBody = $('#clients-table tbody');
            tableBody.on('click', 'tr', function () {
                var data = tableAjax.row( this ).data();
                window.location.href = BlankonApp.handleBaseURL() + 'app/client/view/' + data['id'] ;
            } );
        }

    };

}();

// Call main app init
ClientsTable.init();